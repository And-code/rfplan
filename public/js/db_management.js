/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// текущая база данных
var current_db_global = null;

//  функция кнопок
// action_type: 'delete', 'clone', 'create', 'update'

function db_manag_buttons_func(action_type, db_name) {

    // блок кода - AJAX
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {

            console.log("db_manag_buttons_func. responseText=" + this.responseText);

            if (this.responseText == null || this.responseText == "") {
                console.log("this.responseText==NULL");
                return;
            }

            var total_JSON = JSON.parse(this.responseText);

            if (total_JSON == null) {
                console.log("total_JSON==NULL");
                return;
            }
            
            var view_message_HTML = total_JSON.view_message;
            console.log("view_message_HTML=" + view_message_HTML);
            
            var message_type = total_JSON.type;
            console.log("message_type=" + message_type);
            
            if (message_type == 'danger') {
                $("#id_div_messages").remove();
                $("#navbar_main").after(view_message_HTML);
                return;
            } else {
                $("#id_div_messages").remove();
                $("#navbar_main").after(view_message_HTML);
            }
            
            var view_db_table_HTML = total_JSON.view_db_table;
            console.log("view_db_table_HTML=" + view_db_table_HTML);
            
            if (view_db_table_HTML != undefined) {
                $("#div_db_table").html(view_db_table_HTML);
                return;
            }
            
//            
//            console.log("JSON.parse(this.responseText)=" + array_from_json.toString());
//
//            var length = Object.keys(array_from_json).length;
//
//            console.log("array_from_json.length=" + length);
//
//            if (array_from_json.db_name !== undefined) {
//                
//                document.getElementById("td_id_"+array_from_json['db_name']).innerHTML = "Да";
//                        
//                document.getElementById("td_id_"+current_db_global).innerHTML = "Нет";
//
//            // эта переменная будет использоваться только для визуальной отметки "Да" или "Нет"
//                    // информация про текущую базу данных сохраянется глобально в $_SESSION
//                console.log("current_db_global1_before=" + current_db_global);
//                
//                current_db_global = array_from_json['db_name'];
//                console.log("current_db_global2_after=" + current_db_global);
//            }
//
//            // обновляем таблицу
//            if (array_from_json.clone !== undefined || 
//                    array_from_json.delete !== undefined ||
//                    array_from_json.create_new !== undefined || 
//                    array_from_json.set_descr !== undefined ||
//                    array_from_json.set_date !== undefined) {
//                document.getElementById("div_db_table").innerHTML = array_from_json['div_table_html'];
//            }
        }
    
        if (this.readyState == 4 && this.status == 500) {
            console.log("showNextRow()=" + this.responseText);

            var total_JSON = JSON.parse(this.responseText);
            console.log("total_JSON=" + total_JSON);

            

            var view_message_HTML = total_JSON.view_message;
            console.log("view_message_HTML=" + view_message_HTML);
            
            var message_type = total_JSON.type;
            console.log("message_type=" + message_type);
            
            if (message_type == 'danger') {
                $("#id_div_messages").remove();
                $("#navbar_main").after(view_message_HTML);
                return;
            } else {
                $("#id_div_messages").remove();
                $("#navbar_main").after(view_message_HTML);
            }
        }
    }
//                }
    // блок кода - AJAX -- конец

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var json = null;
       
    if (action_type == 'update') {
        json = JSON.stringify({
            _token: CSRF_TOKEN,
            action_type: action_type,
            db_name: db_name,
            alias: $('#p_set_table_name_'+db_name).val(),
            rfplan_date: $('#p_set_date_id_'+db_name).val(),
            plan_release_descr: $('#p_set_descr_id_'+db_name).val()
        });
    } else { // 'clone', 'create', 'delete'
        
        
        var locale = window.location.pathname.split('/')[1];
        message = locale != "ua" ? "Удалить План частот?" : "Видалити План частот?";
        
        if (action_type == 'delete' && !window.confirm(message)) {
                return;   
        }
        
        json = JSON.stringify({
            _token: CSRF_TOKEN,
            action_type: action_type,
            db_name: db_name
        });
        
    }
    
    

    console.log("db_manag_buttons_func(): json_of_POST= " + json);

    
//    xmlhttp.open("POST", "/admin/db_manag", true);
    xmlhttp.open("POST", window.location.href, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

    //xmlhttp.send();

    // Отсылаем объект в формате JSON и с Content-Type application/json
    // Сервер должен уметь такой Content-Type принимать и раскодировать
    xmlhttp.send(json);
}
   
   

