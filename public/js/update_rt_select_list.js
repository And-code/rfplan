/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



// функция изменения списка радиотехнологий в выпадающем списке при изменении выбора чекбоксов
function update_rt_select_list(is_db_changing) {


    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {

            document.getElementById("select_rt_div").innerHTML = this.responseText;
            
            console.log("update_rt_select_list= ок");
            $(document).ready(function(){
                console.log("$(\"#search_form\").submit()");
              $("#search_form").submit();
            });
            
        }

    }

    // проверяем какие чекбоксы отмечены
    var ZK_active = document.getElementById("ZK_active").checked ? 1 : 0;
    var ZK_perspective = document.getElementById("ZK_perspective").checked ? 1 : 0;
    var SK_active = document.getElementById("SK_active").checked ? 1 : 0;
    var SK_perspective = document.getElementById("SK_perspective").checked ? 1 : 0;

    // получаем номер выбранной радиотехнологии в выпадающем списке
    var rt_numb_selected = 0;
    if ((typeof is_db_changing === 'undefined') || !is_db_changing) {
        rt_numb_selected = document.getElementById("rt_choice").value; // готовое поле для GET
    } 

    var strURL = "get_rt_list_1.php?ZK_active=" + ZK_active +
            "&ZK_perspective=" + ZK_perspective +
            "&SK_active=" + SK_active +
            "&SK_perspective=" + SK_perspective +
            "&" + rt_numb_selected;

    xmlhttp.open("GET", strURL, true);
//                xmlhttp.open("POST", "./get_from_db_json_responce.php", true);
//                xmlhttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

    xmlhttp.send();

    // Отсылаем объект в формате JSON и с Content-Type application/json
    // Сервер должен уметь такой Content-Type принимать и раскодировать
    //xmlhttp.send(json);
}


