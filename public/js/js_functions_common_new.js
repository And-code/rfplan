/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//   очистить поля частот
function clear_freq_filelds() {
    document.getElementById("rf_start_mhz").value = 0;
    document.getElementById("rf_stop_mhz").value = 0;
    document.getElementById("freq_from_unit").selectedIndex = 2;
    document.getElementById("freq_to_unit").selectedIndex = 2;
}

function clear_freq_fields_numbered(field_numb) {
    field_numb = isNaN(field_numb) ? "" : field_numb;
    field_numb = field_numb === 0 ? "" : field_numb;
    document.getElementById("rf_start_mhz" + field_numb).value = 0;
    document.getElementById("rf_stop_mhz" + field_numb).value = 0;
    document.getElementById("freq_from_unit" + field_numb).selectedIndex = 2;
    document.getElementById("freq_to_unit" + field_numb).selectedIndex = 2;
}



//        обновление данных на странице по кнопкам "первая", "последняя", "предыдущая", "следующая" запись
// $row_type_to_get = ('prev', 'next', 'min', 'max', 'delete_and_prev', 'exect_rt_numb')
function showNextRowAjax(url_link, row_type_to_get = null, current_page_numb = 0) {

//
    function update_list_on_delete(array_from_json, view_rt_list_HTML) {

        var currentPage_numb = 0;
        if (array_from_json != null && array_from_json.total != 0) {
            currentPage_numb = array_from_json.current_page;
        }
        console.log("currentPage_numb =" + currentPage_numb);

        $('#select_rt_div').html(view_rt_list_HTML);
        $('#rt_choice').val(currentPage_numb);
    }

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {

//            ['rt_row_JSON' => $rt_row_JSON, 'frequencies_JSON' => $frequencies_JSON]

            console.log("showNextRow()=" + this.responseText);

            var total_JSON = JSON.parse(this.responseText);
            console.log("total_JSON=" + total_JSON);



            var view_message_HTML = total_JSON.view_message;
            console.log("view_message_HTML=" + view_message_HTML);

            var message_type = total_JSON.message_type;
            console.log("message_type=" + message_type);

            if (message_type == 'danger') {
                $("#id_div_messages").remove();
                $("#navbar").after(view_message_HTML);
                return;
            } else {
                $("#id_div_messages").remove();
                $("#navbar").after(view_message_HTML);
            }

            var rt_row_JSON = total_JSON.rt_row_JSON;
            console.log("rt_row_JSON=" + rt_row_JSON);
            var frequencies_HTML = total_JSON.frequencies_HTML;
            console.log("frequencies_HTML=" + frequencies_HTML);

            var view_rt_list_HTML = total_JSON.view_rt_list;
            console.log("view_rt_list_HTML=" + view_rt_list_HTML);

            var array_from_json = JSON.parse(rt_row_JSON);

            if (array_from_json == null) {
                console.log("array_from_json==NULL");
                return;
            }

            var length = Object.keys(array_from_json).length;

            console.log("array_from_json.length=" + length);

            if (array_from_json != null) {

//                $('#first_page').attr("onclick", 'showNextRowAjax(\'' + array_from_json.first_page_url + '\')');
//                $('#prev_page').attr("onclick", 'showNextRowAjax(\'' + array_from_json.prev_page_url + '\')');
//                $('#next_page').attr("onclick", 'showNextRowAjax(\'' + array_from_json.next_page_url + '\')');
//                $('#last_page').attr("onclick", 'showNextRowAjax(\'' + array_from_json.last_page_url + '\')');
                $('.first_page').attr("onclick", 'showNextRowAjax(\'' + array_from_json.first_page_url + '\')');
                $('.prev_page').attr("onclick", 'showNextRowAjax(\'' + array_from_json.prev_page_url + '\')');
                $('.next_page').attr("onclick", 'showNextRowAjax(\'' + array_from_json.next_page_url + '\')');
                $('.last_page').attr("onclick", 'showNextRowAjax(\'' + array_from_json.last_page_url + '\')');

//                $('#delete_row').attr("onclick", 'showNextRowAjax(\'' + window.location + '\', \'delete\', ' + array_from_json.current_page + ')');
                $('.delete_row').attr("onclick", 'showNextRowAjax(\'' + window.location + '\', \'delete\', ' + array_from_json.current_page + ')');

                if (row_type_to_get == 'delete' || row_type_to_get == 'update' || row_type_to_get == 'save_as_new') {
                    update_list_on_delete(array_from_json, view_rt_list_HTML);
                } else {
                    var currentPage_numb = array_from_json.current_page;
                    console.log("currentPage_numb =" + currentPage_numb);

                    $('#rt_choice').val(currentPage_numb);
                }

                if (row_type_to_get == 'update') {
                    return;
                }

                if (array_from_json.data[0] != null) {

                    document.getElementById("id_of_entry").innerHTML = array_from_json.data[0]['id'];

                    console.log("showNextRow()= array_from_json['rt_numb'] = " + array_from_json.data[0]['rt_numb']);
                    document.getElementById("rt_numb").value = array_from_json.data[0]['rt_numb'];

                    document.getElementById("freq_subgroup_numb").value = array_from_json.data[0]['freq_subgroup_numb'];

                    document.getElementById("rt_name").value = array_from_json.data[0]['rt_name'];

                    document.getElementById("rs_name").value = array_from_json.data[0]['rs_name'];

                    document.getElementById("rcom_type") != null ? document.getElementById("rcom_type").value = array_from_json.data[0]['rcom_type'] : null;
                    document.getElementById("std_base").value = array_from_json.data[0]['std_base'];

                    document.getElementById("std_gen") != null ? document.getElementById("std_gen").value = array_from_json.data[0]['std_gen'] : null;

                    document.getElementById("other_norms") != null ? document.getElementById("other_norms").value = array_from_json.data[0]['other_norms'] : null;

                    document.getElementById("rt_freq_all").value = array_from_json.data[0]['rt_freq_all'];

                    document.getElementById("freqDiv").innerHTML = frequencies_HTML;

                    document.getElementById("rt_use_features").value = array_from_json.data[0]['rt_use_features'];

                    document.getElementById("rt_persp_usestart_date") != null ? document.getElementById("rt_persp_usestart_date").value = array_from_json.data[0]['rt_persp_usestart_date'] : null;
                    document.getElementById("rt_usestop_date") != null ? document.getElementById("rt_usestop_date").value = array_from_json.data[0]['rt_usestop_date'] : null;


                    document.getElementById("rt_notes").value = array_from_json.data[0]['rt_notes'];
                    document.getElementById("gov_accept_date").value = array_from_json.data[0]['gov_accept_date'];
                } else {

                    if (row_type_to_get == 'delete') {
                        update_list_on_delete(array_from_json, view_rt_list_HTML);
                    }

                    $('#id_of_entry').html("");
                    $('#rt_numb').val("");
                    $('#freq_subgroup_numb').val("");
                    $('#rt_name').val("");
                    $('#rs_name').val("");
                    $('#rcom_type').val("");
                    $('#std_base').val("");
                    $('#std_gen').val("");
                    $('#other_norms').val("");
                    $('#rt_freq_all').val("");
                    $('#rt_use_features').val("");
                    $('#rt_persp_usestart_date').val("");
                    $('#rt_usestop_date').val("");
                    $('#rt_notes').val("");
                    $('#gov_accept_date').val("");


                    document.getElementById("freqDiv").innerHTML = frequencies_HTML;

                }
            }
        }

        if (this.readyState == 4 && this.status == 500) {
            console.log("showNextRow()=" + this.responseText);

            var total_JSON = JSON.parse(this.responseText);
            console.log("total_JSON=" + total_JSON);



            var view_message_HTML = total_JSON.view_message;
            console.log("view_message_HTML=" + view_message_HTML);

            var message_type = total_JSON.message_type;
            console.log("message_type=" + message_type);

            if (message_type == 'danger') {
                $("#id_div_messages").remove();
                $("#navbar").after(view_message_HTML);
                return;
            } else {
                $("#id_div_messages").remove();
                $("#navbar").after(view_message_HTML);
            }
        }
    }

    if (url_link == "#") {
        return;
    }
    console.log("url_link: " + url_link);

    row_type_to_get_change = ['delete', 'update', 'save_as_new'];



    if (row_type_to_get_change.includes(row_type_to_get)) {

        console.log("row_type_to_get: " + row_type_to_get);

        if (row_type_to_get == 'delete' && !window.confirm("Видалити запис?")) {
            return;
        }

        if (row_type_to_get == 'update' && !window.confirm("Оновити запис?")) {
            return;
        }

//         получаем номер (entry_id) выбранной радиотехнологии в выпадающем списке
        var exect_rt_id = $('#id_of_entry').html();

        if (exect_rt_id == 0 && row_type_to_get != "save_as_new") {
            console.log("try to update empty row: break");
            return;
        }

//     переделать выпадающий список под номера страниц, а не номера радиотехнологий

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');



        var form_var = $('#qqq');
        var form_data = form_var.serialize();
        console.log("form_data: " + form_data);

        var json = JSON.stringify({
            _token: CSRF_TOKEN,
            row_type_to_get: row_type_to_get,
            url_link: url_link,
            exect_rt_id: exect_rt_id,
            current_page_numb: current_page_numb,
            form_data: form_data
        });
        console.log("json: " + json);


        xmlhttp.open("POST", url_link, true);
        xmlhttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        xmlhttp.send(json);

    } else {
        // доделать удаление и т.д.
        xmlhttp.open("GET", url_link, true);
        xmlhttp.send();

}
}


//          Глобальная переменная для функции addFreqField()
var field_numb = 0;

function addFreqField() {


    function create_div_freq_text(field_numb, childId) {

        // локализация
        var localization = {
            "from": $('#from').html(),
            "to": $('#to').html(),
            "clear": $('#clear_freq').val(),
            "delete": $('#delete_freq_btn').html(),
        };

        // для div_freq
        var freq_from_unit = "freq_from_unit" + field_numb;
        var freq_to_unit = "freq_to_unit" + field_numb;
        
        var from_key = "from" + field_numb;
        var to_key = "to" + field_numb;
        
        var rf_start_mhz_key = "rf_start_mhz" + field_numb;
        var rf_stop_mhz_key = "rf_stop_mhz" + field_numb;

        // для div_freq
        // создание полей ввода частот
        var text_freq =
//                "<div class=\"col my-1\">" +
                "<div class=\"row\" id=\"" + childId + "\">" +
                    "<div class=\"col-lg pt-1 input-group\">" +
                        "<div class=\"input-group-prepend\">" +
                            "<span class=\"input-group-text\" id=\""+from_key+"\">" + localization.from + "</span>" +
                        "</div>" +
                    
                        "<input class=\"form-control\" type=\"text\" " +
                                "name=\"" + rf_start_mhz_key + "\" " +
                                "id=\"" + rf_start_mhz_key + "\" " +
                                "value=\"0\" "+
                                "onClick=\"this.setSelectionRange(0, this.value.length)\"> " +
                    
                        "<select class=\"btn btn-info\" name=\"" + freq_from_unit + "\" id=\"" + freq_from_unit + "\">" +
                            "<option value=\"Гц\" >Гц</option>" +
                            "<option value=\"кГц\" >кГц</option>" +
                            "<option value=\"МГц\" selected >МГц</option>" +
                            "<option value=\"ГГц\" >ГГц</option>" +
                        "</select>" +
                    
                    "</div>" +
                    
                    "<div class=\"col-lg pt-1 input-group\">" +
                        "<div class=\"input-group-prepend\">" +
                            "<span class=\"input-group-text\" id=\""+ to_key +"\">" + localization.to + "</span>" +
                        "</div>" +
                    
                        "<input class=\"form-control\" type=\"text\" " +
                                "name=\"" + rf_stop_mhz_key + "\" " +
                                "id=\"" + rf_stop_mhz_key + "\" " +
                                "value=\"0\" "+
                                "onClick=\"this.setSelectionRange(0, this.value.length)\"> " +
                            
                        "<select class=\"btn btn-info\" name=\"" + freq_to_unit + "\" id=\"" + freq_to_unit + "\">" +
                            "<option value=\"Гц\" >Гц</option>" +
                            "<option value=\"кГц\" >кГц</option>" +
                            "<option value=\"МГц\" selected >МГц</option>" +
                            "<option value=\"ГГц\" >ГГц</option>" +
                        "</select>" +
                            
                    "</div>" +
                    
                    "<div class=\"col-lg-auto pt-1\">" +
                    
                        "<input class=\"btn btn-info\" "+
                                "type=\"button\" "+
                                "name=\"clear_freq" + field_numb + "\" "+
                                "value=\"" + localization.clear + "\" " +
                                "onclick=\"clear_freq_fields_numbered(" + field_numb + ")\"> " +

                        "<span class=\"btn btn-info\" "+
                                "onclick=\"var child = document.getElementById(\'" + childId + 
                                "\'); child.parentNode.removeChild(child);\">" + localization.delete + "</span>" +
                    
                    "</div>" +
                "</div>";
                    
                    
                    
                    


//        var button_remove = "<span class=\"btn btn-info\" onclick=\"var child = document.getElementById(\'" +
//                childId + "\'); child.parentNode.removeChild(child);\">" + localization.delete + "</span>";
//
//        text_freq += button_remove;
//        text_freq += "</div>";
//                text_freq += "</div>";

        return text_freq;
    }

    // freqDiv - глобальный div для полей ввода и удаления частот
    var element = document.getElementById("freqDiv");

    // список всех созданных div (div_freq) для ввода и удаления частот 
    // проверяем сколько уже было дочерних элементов. 
    // Если количество дочерних элементов было больше, чем осталось, 
    // то используем как номер поля увеличенное на 1 значение field_numb
    var childElementCount = element.childElementCount;
    field_numb = childElementCount <= field_numb ? ++field_numb : childElementCount;

    var childId = "www" + field_numb;

    // общий div для полей ввода частот
    var div_freq = document.createElement("div");

    // для div_freq_field
//    var att_id = document.createAttribute("id");
//    att_id.value = childId;
//    div_freq.setAttributeNode(att_id);

    // для div_freq_field
    var att_class = document.createAttribute("class");
    att_class.value = "col my-1";
//    att_class.value = "row";
    div_freq.setAttributeNode(att_class);


    div_freq.innerHTML = create_div_freq_text(field_numb, childId);


    element.appendChild(div_freq);


}

            