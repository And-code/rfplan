
$(document).ready(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("#submit").click(function () {
        $.ajax({
            /* the route pointing to the post function */
//            url: '/',
            url: window.location.href,
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, 
                search_text: $("#search_text").val(),
                rf_start_mhz: $("#rf_start_mhz").val(),
                freq_from_unit: $("#freq_from_unit").val(),
                rf_stop_mhz: $("#rf_stop_mhz").val(),
                freq_to_unit: $("#freq_to_unit").val(),
                rt_choice: $("#rt_choice").val(),
                db_choice: $("#db_choice").val(),
                
                ZK_active: $("#ZK_active").prop("checked"),
                SK_active: $("#SK_active").prop("checked"),
                ZK_perspective: $("#ZK_perspective").prop("checked"),
                SK_perspective: $("#SK_perspective").prop("checked")
            },
            dataType: 'JSON',
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {
                console.log("ajax response");
//                $("#qqq").append(data.msg.toString());
//                $("#qqq").html(data.msg);
                $("#rf_plan_table").html(data.msg);
//                console.log("data.db_descr: " + data.db_descr);
                $("#span_db_descr_id").html(data.db_descr);
                // обновляем список радиотехнологий, если он обновляется
                data.rt_list != null ? $("#select_rt_div").html(data.rt_list) : false;
                
//                $("#error_div").text("").addClass("d-none");
                $("#error_div").remove();
            },
            error: function (request, status, error) {
//                alert(request.responseText);
//                $("#error_div").removeClass("d-none").text(request.responseText);

//                console.log("ajax error");
//                console.log("errors" + request.responseJSON.toString());


                $("#error_div").remove();

                var str = "";
                for(var field in request.responseJSON.errors) {
                    var data = request.responseJSON.errors[field];
                    console.log("ajax error data:" + data);
                    
                    if (str == "") {
                        str += data;
                    } else {
                        str += "<br>" + data;
                    }  
                    
                };
                
                var str1 = "<div class=\"alert alert-danger\" id=\"error_div\">";
        
                str1 += str;
                str1 += "</div>";
                
                $("#rfequencies_fields").prepend(str1);
                
                
                
            }
        });
    });
    
    // назначаем отправку формы по Ajax по нажатию Enter
    setSubminOnEnter("search_text");
    setSubminOnEnter("rf_start_mhz");
    setSubminOnEnter("rf_stop_mhz");
    
    // обновление списка доступных радиотехнологий в соответствии с выбранными чекбоксами при перезагрузке страницы
    update_rt_list_by_checkbox_on_reload();
});

// инициирование нажатия кнопки "Поиск" при нажатии клавиши Enter на текущем поле
function setSubminOnEnter(id_of_selector) {
    $( "#" +id_of_selector ).keypress(function( event ) {
        if ( event.which == 13 ) {
            event.preventDefault();
            $("#submit").click();
        }
    });
}

// обновление списка доступных радиотехнологий при нажатии чекбокса
function update_rt_list_by_checkbox(id_of_checkbox, css_class_of_option, reset_rt_list_flag) {
    
    $('.' + css_class_of_option).attr('disabled', !($('#'+id_of_checkbox).prop('checked')));
    
    if (reset_rt_list_flag == true)   {
        $('#rt_choice').val(0); 
        search_onchange();
    }   
}

// обновление списка доступных радиотехнологий в соответствии с выбранными чекбоксами при перезагрузке страницы
function update_rt_list_by_checkbox_on_reload() {
    id_of_checkbox = ['ZK_active', 'SK_active', 'ZK_perspective', 'SK_perspective'];
    css_class_of_option = ['class11', 'class01', 'class10', 'class00'];
    reset_rt_list_flag = false;
    
    update_rt_list_by_checkbox(id_of_checkbox[0], css_class_of_option[0], reset_rt_list_flag);
    update_rt_list_by_checkbox(id_of_checkbox[1], css_class_of_option[1], reset_rt_list_flag);
    update_rt_list_by_checkbox(id_of_checkbox[2], css_class_of_option[2], reset_rt_list_flag);
    update_rt_list_by_checkbox(id_of_checkbox[3], css_class_of_option[3], reset_rt_list_flag);
    
}

// функция мгновенного поиска при изменении выпадающего списка
function search_onchange() {

//    document.getElementById("search_form").submit();
    $('#submit').click();

}