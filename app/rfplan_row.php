<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rfplan_row extends Model
{
    //
    protected $table = "rfplan_rows";
    
    protected $fillable = ['rt_numb', 'freq_subgroup_numb', 'rt_name', 
            'rs_name', 'rcom_type', 'std_base', 
            'std_gen', 'other_norms', 'rt_freq_all', 
            'rt_use_features', 'rt_usestop_date', 
            'rt_persp_usestart_date', 'rt_notes', 
            'gov_accept_date', 'user_type', 
            'rt_active_perspect_type'];
    
    public function __construct($type = null) {

        parent::__construct();

        $this->setTable($type);
    }
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
//    protected $casts = [
//        'gov_accept_date' => 'date',
//    ];
    
    
    public function rt_freq_values() {
        return $this->hasMany('App\rt_freq_value', 'entry_id', 'id');
    }
   
}
