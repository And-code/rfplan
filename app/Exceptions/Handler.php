<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $fullUrl = $request->fullUrl();
        
        $not_costum_errors_path = [route('main', app()->getLocale()), 
            route('login', app()->getLocale()),
            route('register', app()->getLocale())];
        
        $bool_result = in_array($fullUrl, $not_costum_errors_path);
        
        
        
//    if ($request->fullUrl() != route('main', app()->getLocale())) {
    if (!$bool_result) {
         if ($exception instanceof QueryException) {
            if ($exception->getCode()==23000) {
                $message = __('Такий запис вже існує!');
            } else {
                $message = __('Зверніться до адміністратора.<br>Деталі:') . $exception->getMessage();
            }
            $type = 'danger';
            
            
               $view_message = \App\Helpers\HelperFunctions::get_operation_message($type, $message);
            
               if ($request->method() != 'POST') {
                   return response()->view('add_rt', [
                                        'message' => $message,
                                        'type' => $type]);
               }
               
               return response()->json(['view_message' => $view_message, "type" => $type], 500);
        }
        
        if ($exception instanceof ValidationException) {
            
            $messages_array = $exception->validator->getMessageBag()->getMessages();
            
            
            $message = "<ul>";
            foreach ($messages_array as $key => $value) {                
                foreach ($value as $sub_value) {
//                    $message .= "<li>" . $key . ": " . $sub_value . "</li>";
                    $message .= "<li>" . $sub_value . "</li>";
                }
            }
            $message .= "</ul>";
            $type = 'danger';
            
               $view_message = \App\Helpers\HelperFunctions::get_operation_message($type, $message);
            

               return response()->json(['view_message' => $view_message, "type" => $type], 500);
        }
    }
       
        
        
        return parent::render($request, $exception);
    }
}
