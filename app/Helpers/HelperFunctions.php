<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Schema;

/**
 * Description of HelperFunctions
 *
 * @author andrey
 */
class HelperFunctions {
    //put your code here

    /**
     * Convert frequencies to MHz with units
     *
     * @param  array  $data - data arrya with fields 'rf_start_mhz', 'rf_stop_mhz', 
     *                      'freq_from_unit', 'freq_to_unit'
     * @return array  $data - data with pairs of frequencies in MHz. 
     *
     */
    public static function convert_rf_to_mhz_array($request_form_data) {

        $data_all = null;

        foreach ($request_form_data as $key => $value) {
            $suffix = null;

            if (strpos($key, 'rf_start_mhz') === 0) {
                $suffix = substr($key, 12);

                $data = ['rf_start_mhz' => $request_form_data['rf_start_mhz' . $suffix],
                    'rf_stop_mhz' => $request_form_data['rf_stop_mhz' . $suffix],
                    'freq_from_unit' => $request_form_data['freq_from_unit' . $suffix],
                    'freq_to_unit' => $request_form_data['freq_to_unit' . $suffix]];

                $data_temp = self::convert_rf_to_mhz($data);
//                $data_all[] = ['rf_start_mhz' => $data_temp['rf_start_mhz'], 
//                               'rf_stop_mhz' => $data_temp['rf_stop_mhz']];
                $data_all[] = ['id' => null,
                    'entry_id' => $request_form_data['id_of_entry'],
                    'rf_start_mhz' => $data_temp['rf_start_mhz'],
                    'rf_stop_mhz' => $data_temp['rf_stop_mhz']];
            }
        }
        return $data_all;
    }

    /**
     * Convert_rf_to_mhz 
     *
     * @param  array  $data - data as resut of $request->all()
     * @return array  $data - data with frequencies in MHz. 
     *
     */
    public static function convert_rf_to_mhz($data) {

        $data['rf_start_mhz'] = empty($data['rf_start_mhz']) ? "0" : $data['rf_start_mhz'];
        $data['rf_stop_mhz'] = empty($data['rf_stop_mhz']) ? "0" : $data['rf_stop_mhz'];



        $data['rf_start_mhz'] = self::get_rf_mhz(abs($data['rf_start_mhz']), $data['freq_from_unit']);
        $data['rf_stop_mhz'] = self::get_rf_mhz(abs($data['rf_stop_mhz']), $data['freq_to_unit']);


        // меняем местами частоты, если они не в правильном порядке
        // если rf_stop_mhz == 0, то считаем, что нужно искать до +бесконечности
        if ($data['rf_start_mhz'] > $data['rf_stop_mhz'] && $data['rf_stop_mhz'] != 0) {

            $tmp = $data['rf_start_mhz'];
            $data['rf_start_mhz'] = $data['rf_stop_mhz'];
            $data['rf_stop_mhz'] = $tmp;
            $tmp = null;
        }


        return $data;
    }

    public static function get_rf_mhz($frequency, $freq_dimension) {


        if (!is_numeric($frequency)) {
            return 0;
        }

        $rf_mhz = 0; // МГц
        switch ($freq_dimension) {
            case "Гц":
                $rf_mhz = $frequency / 1000000;
                break;
            case "кГц":
                $rf_mhz = $frequency / 1000;
                break;
            case "МГц":
                $rf_mhz = $frequency;
                break;
            case "ГГц":
                $rf_mhz = $frequency * 1000;
                break;
        }

        return $rf_mhz; // MHz
    }

//    return filtered collection
    public static function filter_collection($rt_rows_all, $user_type, $rt_active_perspect_type) {

        $rt_rows_filtered = $rt_rows_all->filter(function($item, $key) use ($user_type, $rt_active_perspect_type) {

            $www = $item->user_type;
            return $item->user_type == $user_type &&
                    $item->rt_active_perspect_type == $rt_active_perspect_type;
        });

        return $rt_rows_filtered;
    }

    // возвращает html код для таблицы с радиотехнологиями по id радиотехнологии
    public static function getHtmlView_by_rt_id($test_query_builder, $user_type, $rt_active_perspect_type) {

        $veiw_resposne = null;
        if ($user_type == 1 && $rt_active_perspect_type == 1) {
            $rt_rows_zk_active = HelperFunctions::filter_collection($test_query_builder, 1, 1);

            $veiw_resposne = view('layouts.rt_active_table', [
                'table_rows' => $rt_rows_zk_active,
                'user_type' => 1,
                'table_style_class' => 'zk_table_style',
                'table_style_class_th_td' => 'zk_table_style_th_td',
                'table_style_class_tr' => 'zk_table_style_tr']
                    )->render();
        } else if ($user_type == 0 && $rt_active_perspect_type == 1) {
            $rt_rows_sk_active = HelperFunctions::filter_collection($test_query_builder, 0, 1);

            $veiw_resposne = view('layouts.rt_active_table', [
                'table_rows' => $rt_rows_sk_active,
                'user_type' => 0,
                'table_style_class' => 'sk_table_style',
                'table_style_class_th_td' => 'sk_table_style_th_td',
                'table_style_class_tr' => 'sk_table_style_tr']
                    )->render();
        } else if ($user_type == 1 && $rt_active_perspect_type == 0) {
            $rt_rows_zk_perspective = HelperFunctions::filter_collection($test_query_builder, 1, 0);

            $veiw_resposne = view('layouts.rt_perspective_table', [
                'table_rows' => $rt_rows_zk_perspective,
                'user_type' => 1,
                'table_style_class' => 'zk_table_style',
                'table_style_class_th_td' => 'zk_table_style_th_td',
                'table_style_class_tr' => 'zk_table_style_tr']
                    )->render();
        } else if ($user_type == 0 && $rt_active_perspect_type == 0) {
            $rt_rows_sk_perspective = HelperFunctions::filter_collection($test_query_builder, 0, 0);

            $veiw_resposne = view('layouts.rt_perspective_table', [
                'table_rows' => $rt_rows_sk_perspective,
                'user_type' => 0,
                'table_style_class' => 'sk_table_style',
                'table_style_class_th_td' => 'sk_table_style_th_td',
                'table_style_class_tr' => 'sk_table_style_tr']
                    )->render();
        }
        return $veiw_resposne;
    }

    // возвращает html код для таблицы с радиотехнологиями для всех радиотехнологий
    public static function getHtmlView($request_data, $test_query_builder) {

        $veiw_resposne = null;
        if ($request_data['ZK_active'] === "true") {
            $rt_rows_zk_active = HelperFunctions::filter_collection($test_query_builder, 1, 1);

            $veiw_resposne .= view('layouts.rt_active_table', [
                'table_rows' => $rt_rows_zk_active,
                'user_type' => 1,
                'table_style_class' => 'zk_table_style',
                'table_style_class_th_td' => 'zk_table_style_th_td',
                'table_style_class_tr' => 'zk_table_style_tr']
                    )->render();
        }
        if ($request_data['SK_active'] === "true") {
            $rt_rows_sk_active = HelperFunctions::filter_collection($test_query_builder, 0, 1);

            $veiw_resposne .= view('layouts.rt_active_table', [
                'table_rows' => $rt_rows_sk_active,
                'user_type' => 0,
                'table_style_class' => 'sk_table_style',
                'table_style_class_th_td' => 'sk_table_style_th_td',
                'table_style_class_tr' => 'sk_table_style_tr']
                    )->render();
        }
        if ($request_data['ZK_perspective'] === "true") {
            $rt_rows_zk_perspective = HelperFunctions::filter_collection($test_query_builder, 1, 0);

            $veiw_resposne .= view('layouts.rt_perspective_table', [
                'table_rows' => $rt_rows_zk_perspective,
                'user_type' => 1,
                'table_style_class' => 'zk_table_style',
                'table_style_class_th_td' => 'zk_table_style_th_td',
                'table_style_class_tr' => 'zk_table_style_tr']
                    )->render();
        }
        if ($request_data['SK_perspective'] === "true") {
            $rt_rows_sk_perspective = HelperFunctions::filter_collection($test_query_builder, 0, 0);

            $veiw_resposne .= view('layouts.rt_perspective_table', [
                'table_rows' => $rt_rows_sk_perspective,
                'user_type' => 0,
                'table_style_class' => 'sk_table_style',
                'table_style_class_th_td' => 'sk_table_style_th_td',
                'table_style_class_tr' => 'sk_table_style_tr']
                    )->render();
        }
        return $veiw_resposne;
    }

    // возвращает HTML код для выпадающего списка
    public static function get_rt_list_HTML($rt_list_arrays) {
        
        $veiw_resposne = view('layouts.rt_list', [
                "rt_list_arrays" => $rt_list_arrays
                ]
                    )->render();
        
        return $veiw_resposne;
        
    }
            
    
    
    // функция формирования html с div с сообщением про ошибку или успех
    // $type - 'success', 'warning', 'danger'
    public static function get_operation_message($type, $message) {

        $veiw_resposne = view('layouts.div_message', [
                'message' => $message,
                'type' => $type])->
                render();
        
        return $veiw_resposne;
    }

    
    public static function update_rfplan_release_description() {
        
        $tabel_rfplan_rows_default_columns = ['id',
                                 'rt_numb',
                                 'freq_subgroup_numb',
                                 'rt_name',
                                 'rs_name',
                                 'rcom_type',
                                 'std_base',
                                 'std_gen',
                                 'other_norms',
                                 'rt_freq_all',
                                 'rt_use_features',
                                 'rt_usestop_date',
                                 'rt_persp_usestart_date',
                                 'rt_notes',
                                 'gov_accept_date',
                                 'user_type',
                                 'rt_active_perspect_type'];
        
        $tabel_rt_freq_values_default_columns = ['id',
                                'entry_id',
                                'rf_start_mhz',
                                'rf_stop_mhz'];
        
        $list_of_tables = DB::select('show tables');

//        $table_name_rfplan_rows = "rfplan_rows";
//        $tabel_name_rt_freq_values = "rt_freq_values";
        
        
        // названия таблиц берем из файлов кофигурации
        $table_name_rfplan_rows = config('app.rf_table_name', "rfplan_rows");
        $tabel_name_rt_freq_values = config('app.freq_table_name', "rt_freq_values");
        
        // список всех доступных таблиц с Планом частот. 
        // Все базы данных имеют абсолютно одинаковую структуру таблиц, но
        // разные названия
        $tabeles = null;
        foreach ($list_of_tables as $key => $value_obj) {
            $table_name = array_values((array) $value_obj)[0];
            
            // проверяем начинается ли имя таблицы с определенного значения
            if (strpos($table_name, $table_name_rfplan_rows) !== false) {
//                $suffix = substr($table_name, 11);
                $suffix = substr($table_name, strlen($table_name_rfplan_rows));
                
                $bool_result1 = Schema::hasColumns($table_name, $tabel_rfplan_rows_default_columns);
                if ($bool_result1) {
                    
                    // находим пару таблицы с частотами
                    foreach ($list_of_tables as $key1 => $value_obj1) {
                            $table_name1 = array_values((array) $value_obj1)[0];
                            // проверяем начинается ли имя таблицы с определенного значегия
                            if ($table_name1 == $tabel_name_rt_freq_values.$suffix) {
                                $bool_result2 = Schema::hasColumns($table_name1, $tabel_rt_freq_values_default_columns);
                                    if ($bool_result2) {
                                        $tabeles[$table_name] = $table_name1;
                                        break;
                                    }
                            }
                        }
                    }
                    
                }
                
                
        }
        
        // создаем список таблиц без описания
        foreach ($tabeles as $key => $value) {
//            \App\rfplan_release_description::firstOrCreate(['rfplan_table_name', $tabeles[$key][0]]);
            \App\rfplan_release_description::firstOrCreate(['rfplan_table_name' => $key], ['alias' => $key, 'rfplan_date'=>__('нема..'), 'plan_release_descr' => __('нема..')]);
        }
        
        // помечаем несуществующие более таблицы в базе данных 
        \App\rfplan_release_description::whereNotIn('rfplan_table_name', array_keys($tabeles))->
                update(['actual' => false]);
        
        // возвращаем самую последнюю по дате строку с данными про План частот
        return \App\rfplan_release_description::where('actual', true)->orderBy('rfplan_date', 'desc')->first();
    }
    
    // обновляем таблицу с описанием Планов частот "rfplan_release_description", сортируем по "rfplan_date"
        // выбираем название самой последней таблицы из списка и сохраняем в переменной сессии 
    public static function checkAndUpdateSessionSelectedTableValue(Request $request) {
        
        if (session()->exists('rfplan_rows')) {
            $bool_res1 = Schema::hasTable(session('rfplan_rows'));
            // проверка есть ли описание плана частот в таблице db_descriptions
            $result_array = \App\rfplan_release_description::where('rfplan_table_name', session('rfplan_rows'))->first();
            $result_bool = empty($result_array);
            // если таблицы не существует (например, ее уже удалили)
            if (!$bool_res1 || $result_bool) {
                $request->session()->forget('selected_table_suffix');
                
                HelperFunctions::updateSessionSelectedTableValue($request);
            }
        } else {
                HelperFunctions::updateSessionSelectedTableValue($request);
        }
        
    }
    
// сортируем таблицу "rfplan_release_description" по "rfplan_date"
// выбираем название самой последней таблицы из списка и сохраняем в переменной сессии 
    public static function updateSessionSelectedTableValue(Request $request) {
        
        // обновляем таблицу с описанием Планов частот
        // сортируем таблицу "rfplan_release_description" по "rfplan_date"
        // выбираем самую последнюю таблицу из списка и загружем ее.
        
                
                $request->session()->get('selected_table_suffix', function(){
                    $rfplan_table_row = HelperFunctions::update_rfplan_release_description();
                    $rfplan_table_name = $rfplan_table_row->rfplan_table_name;
                    
                    $suffix = HelperFunctions::setSessionSelectedTableValue($rfplan_table_name); 
                    
                    return $suffix;
                });
           
    }
    
    public static function setSessionSelectedTableValue($rfplan_table_name) {
                    $suffix = substr($rfplan_table_name, 11); //rfplan_rows
                    
                    session(['selected_table_suffix' => $suffix, 
                        'rfplan_rows' => 'rfplan_rows'.$suffix,
                        'rt_freq_values' => 'rt_freq_values'.$suffix
                        ]);
                    return $suffix;
    }
    
    public static function searchFunction($request_data_with_converted_freq, 
                                        $rfplan_row_by_exect_id, 
                                        $rfplan_row_var,
                                        $table_name_rfplan_rows,
                                        $table_name_rt_freq_values) {
        
        // меняем местами частоты ОТ и ДО, если необходимо
        // если частота ДО = 0, то считаем, что исчем до +бесконечности
//        $request_data = HelperFunctions::convert_rf_to_mhz($request_data);

        // поиск по базе данных по выбранным параметрам запроса
        $rf_start_mhz = $request_data_with_converted_freq['rf_start_mhz'];
        $rf_stop_mhz = $request_data_with_converted_freq['rf_stop_mhz'];
        $search_request = $request_data_with_converted_freq['search_text']; // search request
        $rt_id = $request_data_with_converted_freq['rt_choice'];

        // поиск данных в базе данных
        $result_of_search_query = $rfplan_row_var->
                        select($table_name_rfplan_rows.".id", "rt_numb", "rt_name", "rs_name", "rcom_type", 
                                "std_base", "std_gen", "other_norms", "rt_freq_all", "rt_use_features", 
                                "rt_usestop_date", "rt_persp_usestart_date", "rt_notes", 
                                "user_type", "rt_active_perspect_type")->
                        join($table_name_rt_freq_values, $table_name_rfplan_rows.'.id', '=', 
                                $table_name_rt_freq_values.'.entry_id')->
                        where(function($query) use($rt_id, $rfplan_row_by_exect_id) {

                                if ($rt_id != 0) {
                                    $query->where("rt_numb", "=", $rfplan_row_by_exect_id->rt_numb)->
                                    where("user_type", "=", $rfplan_row_by_exect_id->user_type)->
                                    where("rt_active_perspect_type", "=", $rfplan_row_by_exect_id->rt_active_perspect_type);
                                }
                            })->
                        where(function($query) use($rf_start_mhz, $rf_stop_mhz) {

                                if (empty($rf_stop_mhz)) {
                                    $query->where('rf_start_mhz', ">=", $rf_start_mhz)->
                                            orWhere('rf_stop_mhz', ">=", $rf_start_mhz);
                                } else {
                                    $query->whereRaw("((rf_start_mhz<=" . $rf_start_mhz . " and rf_stop_mhz>=" . $rf_stop_mhz . ") OR "
                                            . "(rf_start_mhz>=" . $rf_start_mhz . " and rf_start_mhz<=" . $rf_stop_mhz . " AND "
                                            . "rf_stop_mhz>=" . $rf_stop_mhz . ") OR "
                                            . "(rf_start_mhz>=" . $rf_start_mhz . " and rf_stop_mhz<=" . $rf_stop_mhz . ") OR "
                                            . "(rf_start_mhz<=" . $rf_start_mhz . " and rf_stop_mhz>=" . $rf_start_mhz . "  and "
                                            . "rf_stop_mhz<=" . $rf_stop_mhz . "))");
                                }
                            })->
                        whereRaw("CONCAT_WS(' ', rt_numb,  rt_name, rs_name, "
                                . "rcom_type, std_base, std_gen, other_norms, "
                                . "rt_freq_all, rt_use_features, "
                                . "rt_usestop_date, rt_persp_usestart_date, rt_notes) LIKE ? ", ["%" . $search_request . "%"])->
                        groupBy($table_name_rfplan_rows.".id")->orderBy('rt_numb')->get();

        return $result_of_search_query;
        
    }
    
}
