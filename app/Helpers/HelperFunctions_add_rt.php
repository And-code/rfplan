<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

use Illuminate\Http\Request;
use App\rfplan_row;
use App\rt_freq_value;
use Validator;

/**
 * Description of HelperFunctions_add_rt
 *
 * @author andrey
 */
class HelperFunctions_add_rt {

    //put your code here


    public static function change_db_row_common($request, $user_type, $rt_active_perspect_type) {

        // динамическое назначение имени таблицы
//        $table_plan_rfplan_rows = $request()->session()->get('rfplan_rows', null);
//        $table_plan_rt_freq_values = $request()->session()->get('rt_freq_values', null);
        $rfplan_row_var = new rfplan_row(session('rfplan_rows', null));
        $rt_freq_var = new rt_freq_value(session('rt_freq_values', null));



        $request_data = $request->except("_token");

        $row_type_to_get = $request_data['row_type_to_get'];

        if ($row_type_to_get == "delete") {

//                $rt_row_delete_result = rfplan_row::where("id", "=", $request_data['exect_rt_id'])->delete();
            $rt_row_delete_result = $rfplan_row_var->where("id", "=", $request_data['exect_rt_id'])->delete();

            $message = 'Delete OK';
            $type = 'success';
            $rt_row_and_freq_JSON = self::update_page_elements($rfplan_row_var, $rt_freq_var, $request_data, $row_type_to_get, $user_type, $rt_active_perspect_type, $message, $type);

            return $rt_row_and_freq_JSON; // возвращаем для JSON запроса
        } else if ($row_type_to_get == "update") {

//                $validation_bool = $this->validateInput($request);
            $validation_bool = self::validateInput($request);

            $update_result_bool = self::update_or_save_db_row($rfplan_row_var, $rt_freq_var, $request_data, $user_type, $rt_active_perspect_type, $row_type_to_get);

            $message = 'Update OK';
            $type = 'success';
            $rt_row_and_freq_JSON = self::update_page_elements($rfplan_row_var, $rt_freq_var, $request_data, $row_type_to_get, $user_type, $rt_active_perspect_type, $message, $type);


            return $rt_row_and_freq_JSON; // возвращаем для JSON запроса
        } else if ($row_type_to_get == "save_as_new") {

//                $validation_bool = $this->validateInput($request);
            $validation_bool = self::validateInput($request);

            $last_insert_id = self::update_or_save_db_row($rfplan_row_var, $rt_freq_var, $request_data, $user_type, $rt_active_perspect_type, $row_type_to_get);

            // Доделать.... вывести сообщение, что успешно сохранили данные в БД
            // указываем id новой добавленной строки
            $request_data['exect_rt_id'] = $last_insert_id;

            $message = 'Save OK';
            $type = 'success';
            $rt_row_and_freq_JSON = self::update_page_elements($rfplan_row_var, $rt_freq_var, $request_data, $row_type_to_get, $user_type, $rt_active_perspect_type, $message, $type);


            return $rt_row_and_freq_JSON; // возвращаем для JSON запроса
        }
    }

    public static function show_add_rt_page_common($request, $user_type, $rt_active_perspect_type) {

        //динамическое назначение имени таблицы
//        $table_plan = "rfplan_rows_backup";
//        $table_plan_rfplan_rows = $request()->session()->get('rfplan_rows', null);
//        $table_plan_rt_freq_values = $request()->session()->get('rt_freq_values', null);
        $rfplan_row_var = new rfplan_row(session('rfplan_rows', null));


//            $rt_row = rfplan_row::where("rfplan_rows.user_type", "=", $user_type)->
//                where("rfplan_rows.rt_active_perspect_type", "=", $rt_active_perspect_type)->
//                orderBy('rt_numb')->
//                orderBy('freq_subgroup_numb')->
//                paginate(1);
        $rt_row = $rfplan_row_var->where("user_type", "=", $user_type)->
                where("rt_active_perspect_type", "=", $rt_active_perspect_type)->
                orderBy('rt_numb')->
                orderBy('freq_subgroup_numb')->
                paginate(1);

        $total = $rt_row->total();

        if ($rt_row->total() == 0) {

            $rt_list = null;
            $frequencies = null;

            $message = "База даних порожня.";
            $type = 'success';

            return view('add_rt', ['rt_list' => $rt_list, 'first_rt' => $rt_row,
                'frequencies' => $frequencies,
                'user_type' => $user_type,
                'rt_active_perspect_type' => $rt_active_perspect_type,
                'fullUrl' => $request->fullUrl(),
                'message' => $message,
                'type' => $type]);
        }

        $id = $rt_row->first()->id;

        //динамическое назначение имени таблицы
//            $table_freq = 'rt_freq_values_backup';
//            $table_freq = null;
        $rt_freq_var = new rt_freq_value(session('rt_freq_values', null));


        // частоты, соответствующие данному id строки радиотехнологии
//            $frequencies = rt_freq_value::where('entry_id', '=', $id)->
//                            orderBy('rf_start_mhz')->get();
        $frequencies = $rt_freq_var->where('entry_id', '=', $id)->
                        orderBy('rf_start_mhz')->get();

        if ($request->fullUrl() == $request->url()) {

//                $rt_list = rfplan_row::select(['rt_name', 'id', 'rt_numb', 'freq_subgroup_numb', 'user_type', 'rt_active_perspect_type'])->
//                        where("rfplan_rows.user_type", "=", $user_type)->
//                        where("rfplan_rows.rt_active_perspect_type", "=", $rt_active_perspect_type)->
//                        orderBy('rt_numb')->
//                        orderBy('freq_subgroup_numb')->
//                        get();
            $rt_list = $rfplan_row_var->select(['rt_name', 'id', 'rt_numb', 'freq_subgroup_numb', 'user_type', 'rt_active_perspect_type'])->
                    where("user_type", "=", $user_type)->
                    where("rt_active_perspect_type", "=", $rt_active_perspect_type)->
                    orderBy('rt_numb')->
                    orderBy('freq_subgroup_numb')->
                    get();
        }

        $message = "OK";
        $type = 'success';


//        if ($request->fullUrl() == "http://rfplan/admin/add_rt_zk_active") {
        if ($request->fullUrl() == $request->url()) {

            return view('add_rt', ['rt_list' => $rt_list, 'first_rt' => $rt_row,
                'frequencies' => $frequencies,
                'user_type' => $user_type,
                'rt_active_perspect_type' => $rt_active_perspect_type,
                'fullUrl' => $request->fullUrl(),
                'message' => $message,
                'type' => $type]);
        } else {

            if ($type == 'danger') {
                $view_message = view('layouts.div_message', ['message' => $message, 'type' => $type])->render();
                $view_message_JSON = json_encode(['view_message' => $view_message, 'message_type' => $type]);

                // будет показано только сообщение про ошибку. Остальные поля формы не обновляются и не меняются
                return $view_message_JSON; // возвращаем для JSON запроса
            }

            $view_frequencies = view('add_rt_layouts.freq_fields_html', ['frequencies' => $frequencies->all()])->render();
            $rt_row_JSON = $rt_row->toJson();

            $rt_row_and_freq_JSON = json_encode(['rt_row_JSON' => $rt_row_JSON,
                'frequencies_HTML' => $view_frequencies,
                'view_message' => null,
                'message_type' => $type]);

            return $rt_row_and_freq_JSON; // возвращаем для JSON запроса
        }
    }

    public static function update_or_save_db_row($rfplan_row_var, $rt_freq_var, $request_data, $user_type, $rt_active_perspect_type, $row_type_to_get) {

        $data_prepared = self::prepareData_for_save_or_update($request_data, $user_type, $rt_active_perspect_type);

        $request_form_data = $data_prepared['request_form_data'];
        $values = $data_prepared['values'];
        $frequencies_pairs_to_save = $data_prepared['frequencies_pairs_to_save'];

        $rt_row_update_result = null;
        $rt_row_save_as_new_result = null;

        if ($row_type_to_get == 'update') {
//                    $rt_row_update_result = rfplan_row::where("id", "=", $request_data['exect_rt_id'])->
            $rt_row_update_result = $rfplan_row_var->where("id", "=", $request_data['exect_rt_id'])->
                    update($values);

//                    $rt_freq_value_delete_result = rt_freq_value::where("entry_id", "=", $request_data['exect_rt_id'])->
            $rt_freq_value_delete_result = $rt_freq_var->where("entry_id", "=", $request_data['exect_rt_id'])->
                    delete();

            $rt_freq_value_update_result = $rt_freq_var->insert($frequencies_pairs_to_save);

            return $rt_row_update_result && $rt_freq_value_update_result;
        } else if ($row_type_to_get == 'save_as_new') {

            $rfplan_new_row = $rfplan_row_var->fill($values);
            $rt_row_save_as_new_result = $rfplan_new_row->save();

            foreach ($frequencies_pairs_to_save as $key => $value) {
                if (array_key_exists('entry_id', $value)) {
                    $frequencies_pairs_to_save[$key]['entry_id'] = $rfplan_new_row->id;
                }
            }

            $rt_freq_value_update_result = $rt_freq_var->insert($frequencies_pairs_to_save);

            return $rfplan_new_row->id;
        }
    }

    // валидация введеных данных для обновления или создания новой записи
    public static function validateInput($request) {
        // получаем данные из формы
        // правила проверки заполненных полей
        $rules = ['rt_numb' => "required|numeric",
            'freq_subgroup_numb' => "required|numeric|integer|min:1",
            'rt_name' => "required",
            'rs_name' => "required",
            'rcom_type' => "",
            'std_base' => "",
            'std_gen' => "",
            'other_norms' => "",
            'rt_freq_all' => "",
            'rt_use_features' => "",
            'rt_persp_usestart_date' => "",
            'rt_usestop_date' => "",
            'rt_notes' => "",
            'gov_accept_date' => ""];

        $request_data = null;
//                $www = $request->only('form_data');
        parse_str($request->only('form_data')['form_data'], $request_data);

//                $request_data = $request->only('form_data_array');
//                $request_data = $request->only('form_data_array');

        $validator = Validator::make($request_data, $rules)->validate();




//                $validatedData = $request->validate($rules);
        // generate JSON response automatically on validation error
        return true;
    }

    public static function prepareData_for_save_or_update($request_data, $user_type, $rt_active_perspect_type) {
        // получаем данные из формы
        $request_form_data = null;
        parse_str($request_data['form_data'], $request_form_data);
        $request_form_data['id_of_entry'] = $request_data['exect_rt_id'];




        // Дата начала действия
        if (empty($request_form_data['gov_accept_date'])) {
            $request_form_data['gov_accept_date'] = null;
//                    $request_form_data['gov_accept_date'] = date('Y-m-d', strtotime(0));
        }



        $values = null;
        if ($rt_active_perspect_type == 1) {
            $values = ['rt_numb' => $request_form_data['rt_numb'],
                'freq_subgroup_numb' => $request_form_data['freq_subgroup_numb'],
                'rt_name' => $request_form_data['rt_name'],
                'rs_name' => $request_form_data['rs_name'],
                'rcom_type' => $request_form_data['rcom_type'],
                'std_base' => $request_form_data['std_base'],
                'std_gen' => $request_form_data['std_gen'],
                'other_norms' => $request_form_data['other_norms'],
                'rt_freq_all' => $request_form_data['rt_freq_all'],
                'rt_use_features' => $request_form_data['rt_use_features'],
                'rt_persp_usestart_date' => "",
                'rt_usestop_date' => $request_form_data['rt_usestop_date'],
                'rt_notes' => $request_form_data['rt_notes'],
                'gov_accept_date' => $request_form_data['gov_accept_date'],
                'user_type' => $user_type,
                'rt_active_perspect_type' => $rt_active_perspect_type
            ];
        } else {
            $values = ['rt_numb' => $request_form_data['rt_numb'],
                'freq_subgroup_numb' => $request_form_data['freq_subgroup_numb'],
                'rt_name' => $request_form_data['rt_name'],
                'rs_name' => $request_form_data['rs_name'],
                'rcom_type' => "",
                'std_base' => $request_form_data['std_base'],
                'std_gen' => "",
                'other_norms' => "",
                'rt_freq_all' => $request_form_data['rt_freq_all'],
                'rt_use_features' => $request_form_data['rt_use_features'],
                'rt_persp_usestart_date' => $request_form_data['rt_persp_usestart_date'],
                'rt_usestop_date' => "",
                'rt_notes' => $request_form_data['rt_notes'],
                'gov_accept_date' => $request_form_data['gov_accept_date'],
                'user_type' => $user_type,
                'rt_active_perspect_type' => $rt_active_perspect_type
            ];
        }


        // готовый массив для команды update в таблице "rt_freq_value"
        $frequencies_pairs_to_save = \App\Helpers\HelperFunctions::convert_rf_to_mhz_array($request_form_data);

        return ['request_form_data' => $request_form_data,
            'values' => $values,
//                        'frequencies_pairs_to_save' => $frequencies_pairs_to_save[0]];
            'frequencies_pairs_to_save' => $frequencies_pairs_to_save];
    }

    //$rfplan_row_var, $rt_freq_var, - объекты моделей (таблиц)
    public static function update_page_elements($rfplan_row_var, $rt_freq_var, $request_data, $row_type_to_get, $user_type, $rt_active_perspect_type, $message, $type) {
        $page = null;
        if ($row_type_to_get == 'delete') {
            $page = $request_data['current_page_numb'] - 1;
        } else {
            // получаем номер страницы с конкретным id
            $id = $request_data['exect_rt_id'];
//            $page = rfplan_row::select('id')->where("rfplan_rows.user_type", "=", $user_type)->
            $page = $rfplan_row_var->select('id')->where("user_type", "=", $user_type)->
                            where("rt_active_perspect_type", "=", $rt_active_perspect_type)->
                            orderBy('rt_numb')->
                            orderBy('freq_subgroup_numb')->get()->search(function ($item, $key) use($id) {
                return $item->id == $id;
            });
            $page++; // увеличиваем номер страницы, т.к. нумерация массива от 0.
        }

        $rt_row = $rfplan_row_var->where("user_type", "=", $user_type)->
                where("rt_active_perspect_type", "=", $rt_active_perspect_type)->
                orderBy('rt_numb')->
                orderBy('freq_subgroup_numb')->
                paginate(1, ['*'], 'page', $page);

        $frequencies = null;
        $rt_list = null;

        if ($rt_row->first() != null) {
            $id = $rt_row->first()->id;

            // частоты, соответствующие данному id строки радиотехнологии
//                    $frequencies = rt_freq_value::where('entry_id', '=', $id)->
            $frequencies = $rt_freq_var->where('entry_id', '=', $id)->
                            orderBy('rf_start_mhz')->get();

//                    $rt_list = rfplan_row::select(['rt_name', 'id', 'rt_numb', 'freq_subgroup_numb', 'user_type', 'rt_active_perspect_type'])->
            $rt_list = $rfplan_row_var->select(['rt_name', 'id', 'rt_numb', 'freq_subgroup_numb', 'user_type', 'rt_active_perspect_type'])->
                    where("user_type", "=", $user_type)->
                    where("rt_active_perspect_type", "=", $rt_active_perspect_type)->
                    orderBy('rt_numb')->
                    orderBy('freq_subgroup_numb')->
                    get();
        }

        $frequencies = $frequencies == null ? null : $frequencies->all();

        $view_rt_list = view('add_rt_layouts.add_rt_divs.rt_list_add_rt_page_selection', ['rt_list' => $rt_list])->render();

        $view_frequencies = view('add_rt_layouts.freq_fields_html', ['frequencies' => $frequencies])->render();
        $rt_row_JSON = $rt_row->toJson();


        $view_message = view('layouts.div_message', ['message' => $message, 'type' => $type])->render();

        $rt_row_and_freq_JSON = json_encode(['rt_row_JSON' => $rt_row_JSON,
            'frequencies_HTML' => $view_frequencies,
            'view_rt_list' => $view_rt_list,
            'view_message' => $view_message,
            'message_type' => $type]);

        return $rt_row_and_freq_JSON; // возвращаем для JSON запроса
    }

}
