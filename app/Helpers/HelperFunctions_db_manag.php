<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Schema;
use App\rfplan_release_description;
use Illuminate\Support\Facades\Validator;


/**
 * Description of HelperFunctions
 *
 * @author andrey
 */
class HelperFunctions_db_manag {
    //put your code here

    /**
     * Convert frequencies to MHz with units
     *
     * @param  array  $data - data arrya with fields 'rf_start_mhz', 'rf_stop_mhz', 
     *                      'freq_from_unit', 'freq_to_unit'
     * @return array  $data - data with pairs of frequencies in MHz. 
     *
     */
    public static function table_delete($request_data) {

        // названия таблиц берем из файлов кофигурации
        $table_name_rfplan_rows = config('app.rf_table_name', "rfplan_rows");
        $tabel_name_rt_freq_values = config('app.freq_table_name', "rt_freq_values");

//        $suffix = substr($request_data['db_name'], 11);
        $suffix = substr($request_data['db_name'], strlen($table_name_rfplan_rows));
        $paired_rt_freq_values_table_name = "rt_freq_values" . $suffix;

        $bool_res1 = Schema::hasTable($request_data['db_name']);
        $bool_res2 = Schema::hasTable($paired_rt_freq_values_table_name);

        // если такие таблицы существуют - удаляем их
        if ($bool_res1 == true && $bool_res2 == true) {
            //              оцениваем количество действующих таблиц
            $tables_count = \App\rfplan_release_description::where('actual', true)->get()->count();

            if ($tables_count == 1) {
                $table_name = DB::table($paired_rt_freq_values_table_name)->truncate();
                $table_name = DB::table($request_data['db_name'])->truncate();

                $message = 'Delete OK';
                $type = 'success';

                $values = ['alias' => $request_data['db_name'],
                    'rfplan_date' => "нема..",
                    'plan_release_descr' => "нема.."
                ];

                // обновляем таблицу описаний
                $tables_count = \App\rfplan_release_description::where('actual', true)->get()->
                                first()->update($values);
            } else {
                Schema::drop($paired_rt_freq_values_table_name);
                Schema::drop($request_data['db_name']);

                $bool_res1 = Schema::hasTable($request_data['db_name']);
                $bool_res2 = Schema::hasTable($paired_rt_freq_values_table_name);

                // если такие таблицы существуют - удаляем их
                if ($bool_res1 == true && $bool_res2 == true) {
                    $message = 'Delete failed';
                    $type = 'danger';
                } else {
                    $message = 'Delete OK';
                    $type = 'success';
                }
            }
        }

        // обновляем таблицу "rfplan_release_description"
        \App\Helpers\HelperFunctions::update_rfplan_release_description();

//                    выбрать другую таблицу, если удалили таблицу, которую только что просматривали/редактировали
        ///////
        if ($request_data['db_name'] == session('rfplan_rows')) {
//            $request->session()->forget('selected_table_suffix');
//            $request->session()->get('selected_table_suffix', function() {
            request()->session()->forget('selected_table_suffix');
            request()->session()->get('selected_table_suffix', function() {
                $rfplan_table_row = HelperFunctions::update_rfplan_release_description();
                $rfplan_table_name = $rfplan_table_row->rfplan_table_name;
                $suffix = substr($rfplan_table_name, 11); //rfplan_rows
                session(['selected_table_suffix' => $suffix,
                    //                'selected_table_date' => $rfplan_table_row->rfplan_date,
                    'rfplan_rows' => 'rfplan_rows' . $suffix,
                    'rt_freq_values' => 'rt_freq_values' . $suffix
                ]);
                return $suffix;
            });
        }


        ///////

        $descriptions = \App\rfplan_release_description::where('actual', true)->get()->all();

        $view_message = view('layouts.div_message', ['message' => $message, 'type' => $type])->render();

        $view_db_table = view('db_manag_layouts.db_table', ['descriptions' => $descriptions])->render();

        $db_mange_result_JSON = null;
        $db_mange_result_JSON = json_encode(['view_db_table' => $view_db_table,
            'view_message' => $view_message,
            'type' => $type]);

        return $db_mange_result_JSON; // возвращаем для JSON запроса
    }

    public static function table_update($request_data) {

        $validation_bool = self::validateDbInput($request_data);

        $values = ['alias' => $request_data['alias'],
            'rfplan_date' => $request_data['rfplan_date'],
            'plan_release_descr' => $request_data['plan_release_descr']
        ];

        $rt_row_update_result = rfplan_release_description::where("rfplan_table_name", "=", $request_data['db_name'])->
                update($values);


        $message = 'Update OK';
        $type = 'success';

        $view_message = view('layouts.div_message', ['message' => $message, 'type' => $type])->render();

        $db_mange_result_JSON = json_encode(['view_message' => $view_message,
            'type' => $type]);

        return $db_mange_result_JSON; // возвращаем для JSON запроса
    }

    public static function table_clone($request_data) {

        $bool_res = Schema::hasTable($request_data['db_name']);

        // названия таблиц берем из файлов кофигурации
        $table_name_rfplan_rows = config('app.rf_table_name', "rfplan_rows");
        $tabel_name_rt_freq_values = config('app.freq_table_name', "rt_freq_values");

//        $suffix = substr($request_data['db_name'], 11);        
//        $paired_rt_freq_values_table_name = "rt_freq_values" . $suffix;
        $suffix = substr($request_data['db_name'], strlen($table_name_rfplan_rows));
        $paired_rt_freq_values_table_name = $tabel_name_rt_freq_values . "" . $suffix;

        if ($bool_res) {
            $date = date("YmdHis");

//            $new_table_name_rfplan_rows = "rfplan_rows" . $date;
//            $new_tabel_name_rt_freq_values = "rt_freq_values" . $date;
            $new_table_name_rfplan_rows = $table_name_rfplan_rows . "" . $date;
            $new_tabel_name_rt_freq_values = $tabel_name_rt_freq_values . "" . $date;

            $bool_new_rfplan_rows_exist = Schema::hasTable($new_table_name_rfplan_rows);
            $bool_new_rt_freq_values_exist = Schema::hasTable($new_tabel_name_rt_freq_values);

            if ($bool_new_rfplan_rows_exist != true && $bool_new_rt_freq_values_exist != true) {

                $create_statement_rfplan_rows = ((array) DB::select("show create table " . $request_data['db_name'])[0])['Create Table'];
                $create_statement_rt_freq_values = ((array) DB::select("show create table " . $paired_rt_freq_values_table_name)[0])['Create Table'];

                $create_statement_rfplan_rows_new = str_replace($request_data['db_name'], $new_table_name_rfplan_rows, $create_statement_rfplan_rows);
                $create_statement_rt_freq_values_new = str_replace($paired_rt_freq_values_table_name, $new_tabel_name_rt_freq_values, $create_statement_rt_freq_values);
                $create_statement_rt_freq_values_new = str_replace($request_data['db_name'], $new_table_name_rfplan_rows, $create_statement_rt_freq_values_new);


                DB::statement($create_statement_rfplan_rows_new);
                DB::statement("INSERT " . $new_table_name_rfplan_rows . " SELECT * FROM " . $request_data['db_name']);

                DB::statement($create_statement_rt_freq_values_new);
                DB::statement("INSERT " . $new_tabel_name_rt_freq_values . " SELECT * FROM " . $paired_rt_freq_values_table_name);
            }
        }

        $bool_new_rfplan_rows_exist = Schema::hasTable($new_table_name_rfplan_rows);
        $bool_new_rt_freq_values_exist = Schema::hasTable($new_tabel_name_rt_freq_values);

        //
        if ($bool_new_rfplan_rows_exist == true && $bool_new_rt_freq_values_exist == true) {


            $message = 'Clone OK';
            $type = 'success';


            // обновляем таблицу "rfplan_release_description"
            \App\Helpers\HelperFunctions::update_rfplan_release_description();

            $cloned_row = rfplan_release_description::where("rfplan_table_name", "=", $request_data['db_name'])->
                    first();

            $values = ['alias' => $cloned_row->alias . "_copy",
                'rfplan_date' => $cloned_row->rfplan_date,
                'plan_release_descr' => $cloned_row->plan_release_descr
            ];

            $rt_row_update_result = rfplan_release_description::where("rfplan_table_name", "=", $new_table_name_rfplan_rows)->
                    update($values);


            $descriptions = \App\rfplan_release_description::where('actual', true)->get()->all();

            $view_message = view('layouts.div_message', ['message' => $message, 'type' => $type])->render();

            $view_db_table = view('db_manag_layouts.db_table', ['descriptions' => $descriptions])->render();


            $db_mange_result_JSON = json_encode(['view_db_table' => $view_db_table,
                'view_message' => $view_message,
                'type' => $type]);

            return $db_mange_result_JSON; // возвращаем для JSON запроса
        }
    }

    public static function table_create($request_data) {

        $table_reference = \App\rfplan_release_description::where('actual', true)->get()->first();

        // названия таблиц берем из файлов кофигурации
        $table_name_rfplan_rows = config('app.rf_table_name', "rfplan_rows");
        $tabel_name_rt_freq_values = config('app.freq_table_name', "rt_freq_values");

//        $suffix = substr($table_reference->rfplan_table_name, 11);
//        $paired_rt_freq_values_table_name = "rt_freq_values" . $suffix;
        $suffix = substr($table_reference->rfplan_table_name, strlen($table_name_rfplan_rows));
        $paired_rt_freq_values_table_name = $tabel_name_rt_freq_values . "" . $suffix;

        $date = date("YmdHis");

//        $new_table_name_rfplan_rows = "rfplan_rows" . $date;
//        $new_tabel_name_rt_freq_values = "rt_freq_values" . $date;

        $new_table_name_rfplan_rows = $table_name_rfplan_rows . "" . $date;
        $new_tabel_name_rt_freq_values = $tabel_name_rt_freq_values . "" . $date;


        $bool_new_rfplan_rows_exist = Schema::hasTable($new_table_name_rfplan_rows);
        $bool_new_rt_freq_values_exist = Schema::hasTable($new_tabel_name_rt_freq_values);

        if ($bool_new_rfplan_rows_exist != true && $bool_new_rt_freq_values_exist != true) {

            $create_statement_rfplan_rows = ((array) DB::select("show create table " . $table_reference->rfplan_table_name)[0])['Create Table'];
            $create_statement_rt_freq_values = ((array) DB::select("show create table " . $paired_rt_freq_values_table_name)[0])['Create Table'];

            $create_statement_rfplan_rows_new = str_replace($table_reference->rfplan_table_name, $new_table_name_rfplan_rows, $create_statement_rfplan_rows);
//            $create_statement_rfplan_rows_new = str_replace("AUTO_INCREMENT=", "AUTO_INCREMENT=0*", $create_statement_rfplan_rows_new);
//            $truncate_statement_rfplan_rows_new = "TRUNCATE TABLE " . $new_table_name_rfplan_rows . "";
            $truncate_statement_rfplan_rows_new = "ALTER TABLE " . $new_table_name_rfplan_rows . " AUTO_INCREMENT = 1";
            
            $create_statement_rt_freq_values_new = str_replace($paired_rt_freq_values_table_name, $new_tabel_name_rt_freq_values, $create_statement_rt_freq_values);
            $create_statement_rt_freq_values_new = str_replace($table_reference->rfplan_table_name, $new_table_name_rfplan_rows, $create_statement_rt_freq_values_new);
//            $create_statement_rt_freq_values_new = str_replace("AUTO_INCREMENT=", "AUTO_INCREMENT=0*", $create_statement_rt_freq_values_new);
            $truncate_statement_rt_freq_values_new = "ALTER TABLE " . $new_tabel_name_rt_freq_values . " AUTO_INCREMENT = 1";
            
//            $truncate_statement_rt_freq_values_new = "TRUNCATE TABLE " . $new_tabel_name_rt_freq_values . "";

//            DB::statement($create_statement_rfplan_rows_new);
//            DB::statement($create_statement_rt_freq_values_new);
//            DB::statement($create_statement_rfplan_rows_and_truncate_new);
//            DB::statement($create_statement_rt_freq_values_and_truncate_new);
            
            DB::transaction(function () use ($create_statement_rfplan_rows_new, 
                                        $create_statement_rt_freq_values_new,
                                        $truncate_statement_rfplan_rows_new,
                                        $truncate_statement_rt_freq_values_new) {
                DB::statement($create_statement_rfplan_rows_new);
                DB::statement($create_statement_rt_freq_values_new);
                
                DB::statement($truncate_statement_rfplan_rows_new);
                DB::statement($truncate_statement_rt_freq_values_new);
            });
            
        }

        // снова проверяем существуют ли таблицы
        $bool_new_rfplan_rows_exist = Schema::hasTable($new_table_name_rfplan_rows);
        $bool_new_rt_freq_values_exist = Schema::hasTable($new_tabel_name_rt_freq_values);

        //
        if ($bool_new_rfplan_rows_exist == true && $bool_new_rt_freq_values_exist == true) {


            $message = 'Create OK';
            $type = 'success';


            // обновляем таблицу "rfplan_release_description"
            \App\Helpers\HelperFunctions::update_rfplan_release_description();

            $descriptions = \App\rfplan_release_description::where('actual', true)->get()->all();

            $view_message = view('layouts.div_message', ['message' => $message, 'type' => $type])->render();

            $view_db_table = view('db_manag_layouts.db_table', ['descriptions' => $descriptions])->render();


            $db_mange_result_JSON = json_encode(['view_db_table' => $view_db_table,
                'view_message' => $view_message,
                'type' => $type]);

            return $db_mange_result_JSON; // возвращаем для JSON запроса
        }
    }

    public static function table_select($request_data) {
        //              выбрать другую таблицу, если удалили таблицу, которую только что просматривали/редактировали
        ///////
        if ($request_data['db_name'] != session('rfplan_rows')) {

             // названия таблиц берем из файлов кофигурации
            $table_name_rfplan_rows = config('app.rf_table_name', "rfplan_rows");

//            $suffix = substr($request_data['db_name'], 11); //rfplan_rows
            $suffix = substr($request_data['db_name'], strlen($table_name_rfplan_rows)); //rfplan_rows
            session(['selected_table_suffix' => $suffix,                
                'rfplan_rows' => 'rfplan_rows' . $suffix,
                'rt_freq_values' => 'rt_freq_values' . $suffix
            ]);

            $descriptions = \App\rfplan_release_description::where('actual', true)->get()->all();

            $message = 'Select OK';
            $type = 'success';

            $view_message = view('layouts.div_message', ['message' => $message, 'type' => $type])->render();

            $view_db_table = view('db_manag_layouts.db_table', ['descriptions' => $descriptions])->render();


            $db_mange_result_JSON = json_encode(['view_db_table' => $view_db_table,
                'view_message' => $view_message,
                'type' => $type]);

            return $db_mange_result_JSON; // возвращаем для JSON запроса
        }
    }

    // валидация введеных данных для обновления или создания нового Плана частот
    public static function validateDbInput($request_data) {

        // правила проверки заполненных полей
//                $rules = ['db_name_new'=> "required|max:32|alpha_dash|unique:rfplan_release_descriptions,rfplan_table_name",
//                            'db_date'=> "required",
//                            'db_descr'=> "required"];
        $rules = ['alias' => "required",
            'rfplan_date' => "required",
            'plan_release_descr' => "required"];


        $validator = Validator::make($request_data, $rules)->validate();

        return true;
    }

}
