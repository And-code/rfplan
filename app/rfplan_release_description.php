<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rfplan_release_description extends Model
{
    //
    protected $table = "rfplan_release_descriptions";
    
    protected $fillable = ['id',
                            'rfplan_table_name',
                            'alias',
                            'rfplan_date',
                            'plan_release_descr',
                            'actual'];
    
    
    
    
    
    
}
