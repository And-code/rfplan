<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\rfplan_row;
use App\rt_freq_value;
use App\Helpers\HelperFunctions;
use DB;
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use App;

class MainPageController extends Controller {

    public function show(Request $request, $locale) {
        
        // обновляем таблицу с описанием Планов частот
        // сортируем таблицу "rfplan_release_description" по "rfplan_date"
        // выбираем самую последнюю таблицу из списка и загружем ее.
        
        HelperFunctions::checkAndUpdateSessionSelectedTableValue($request);        
        
        $table_name_rfplan_rows = session('rfplan_rows');
        
        // создаем объект модели для таблицы с конкретным названием 
        // (конкретная версия Плана частот)
        $rfplan_row_var = new rfplan_row($table_name_rfplan_rows);
        
        // загрузить данные для списка радиотехнологий
//        $rt_list_arrays = rfplan_row::select(['rt_name', 'id', 'rt_numb', 'user_type', 'rt_active_perspect_type'])->
        $rt_list_arrays = $rfplan_row_var->select(['rt_name', 'id', 'rt_numb', 'user_type', 'rt_active_perspect_type'])->
                where('freq_subgroup_numb', 1)->
                orderBy('rt_active_perspect_type', 'desc')->
                orderBy('user_type', 'desc')->
                orderBy('rt_numb')->
                get();


        //$user_type: 1 = ЗК, 0 = SK
        //$rt_active_perspect_type: 1 = діючі, 0 - перспективні

        // формируем данные для таблиц Плана частот по видам пользователей
        $rt_rows_all = $rfplan_row_var->get();
//        $rt_rows_all = rfplan_row::all();

        $rt_rows_zk_active = HelperFunctions::filter_collection($rt_rows_all, 1, 1);
        $rt_rows_sk_active = HelperFunctions::filter_collection($rt_rows_all, 0, 1);
        $rt_rows_zk_perspective = HelperFunctions::filter_collection($rt_rows_all, 1, 0);
        $rt_rows_sk_perspective = HelperFunctions::filter_collection($rt_rows_all, 0, 0);

        // данные для выпадающего списка Планов частот по дате
        $rf_plan_list = \App\rfplan_release_description::select(['rfplan_table_name','plan_release_descr','rfplan_date'])->
                where('actual', true)->orderBy('rfplan_date', 'desc')->get();
        
        $db_descr = null;
        foreach ($rf_plan_list as $key => $value) {
            $db_descr[$key] = ["rfplan_table_name" => $value->rfplan_table_name, 
//                            "plan_release_descr" => $value->plan_release_descr];
                            "plan_release_descr" => nl2br($value->plan_release_descr) ];
        }
        session(['db_descr' => $db_descr]);
         
        
        return view('index_main', ["rt_list_arrays" => $rt_list_arrays,
            "rt_rows_zk_active" => $rt_rows_zk_active,
            "rt_rows_sk_active" => $rt_rows_sk_active,
            "rt_rows_zk_perspective" => $rt_rows_zk_perspective,
            "rt_rows_sk_perspective" => $rt_rows_sk_perspective,
            "rf_plan_list" => $rf_plan_list
        ]);
    }

    public function showSearch(Request $request, $locale) {

        
        // правила проверки заполненных полей
        $rules = [
            "rf_start_mhz" => "nullable|numeric",
            "rf_stop_mhz" => "nullable|numeric"
        ];


        $request->validate($rules);
        // generate JSON response automatically on validation error
        //если валидация проходит нормально, то выполняем поиск и обновление элементов страницы
        
        
        // проверяем изменилась ли база данных
        $request_data = $request->except("_token");
        
        // если в выпадающем списке релизов Планов частот выбрана новая база данных
        $table_name_rfplan_rows_old = session('rfplan_rows', null);
        $table_name_rfplan_rows_new = session('db_descr')[$request_data['db_choice']]['rfplan_table_name'];
        if ($table_name_rfplan_rows_new != $table_name_rfplan_rows_old ) {
            
                HelperFunctions::setSessionSelectedTableValue($table_name_rfplan_rows_new);
               // обновить список радиотехнологий
                
            
        }
        
        $db_descr = session('db_descr')[$request_data['db_choice']]['plan_release_descr'];
                
        // имена таблиц из которых нужно брать данные Плана частот
        $table_name_rfplan_rows = session('rfplan_rows', null);
        $table_name_rt_freq_values = session('rt_freq_values', null);

        // ORM объекты таблиц
        $rfplan_row_var = new rfplan_row($table_name_rfplan_rows);
        $rt_freq_var = new rt_freq_value($table_name_rt_freq_values);

        // меняем местами частоты ОТ и ДО, если необходимо
        // если частота ДО = 0, то считаем, что исчем до +бесконечности

        $request_data_with_converted_freq = HelperFunctions::convert_rf_to_mhz($request_data);
        
        if ($table_name_rfplan_rows_new != $table_name_rfplan_rows_old ) {
        // загрузить данные для списка радиотехнологий для новой таблицы с Планом частот
            $rt_list_arrays = $rfplan_row_var->select(['rt_name', 'id', 'rt_numb', 'user_type', 'rt_active_perspect_type'])->
                                                    where('freq_subgroup_numb', 1)->
                                                    orderBy('rt_active_perspect_type', 'desc')->
                                                    orderBy('user_type', 'desc')->
                                                    orderBy('rt_numb')->
                                                    get();
            $rt_id = 0;
            $request_data_with_converted_freq['rt_choice'] = $rt_id;
        } else {
            // поиск по базе данных по выбранным параметрам запроса
            $rt_id = $request_data_with_converted_freq['rt_choice'];
            $rt_list_arrays = null;
        }
        
        

        // получем из БД поля "rt_numb", "user_type", "rt_active_perspect_type" 
        // по id конкретной радиотехнологии из выпадаюего списка радиотехнологий на главной странице
        $rfplan_row_by_exect_id = null;
        if ($rt_id != 0) {
            $rfplan_row_by_exect_id = $rfplan_row_var->select("rt_numb", "user_type", "rt_active_perspect_type")
                            ->find($rt_id);
        }


        // поиск данных в базе данных
        $result_of_search_query = HelperFunctions::searchFunction($request_data_with_converted_freq, 
                                        $rfplan_row_by_exect_id, 
                                        $rfplan_row_var,
                                        $table_name_rfplan_rows,
                                        $table_name_rt_freq_values);
                            
        $veiw_resposne = null;
        if ($rt_id != 0 && !empty($rfplan_row_by_exect_id)) {
            $veiw_resposne = HelperFunctions::getHtmlView_by_rt_id($result_of_search_query, 
                                $rfplan_row_by_exect_id->user_type, $rfplan_row_by_exect_id->rt_active_perspect_type);
            $veiw_resposne_rt_list = null;
        
            
        } else {
            $veiw_resposne = HelperFunctions::getHtmlView($request_data_with_converted_freq, $result_of_search_query);
            // формируем новый view для списка радиотехнологий            
            $veiw_resposne_rt_list = $rt_list_arrays != null ? HelperFunctions::get_rt_list_HTML($rt_list_arrays) : null;
            
        }

        $response = array(
            "status" => "success",
            "msg" => $veiw_resposne,
            "db_descr" => $db_descr,
            "rt_list" => $veiw_resposne_rt_list
        );

        return response()->json($response);
        
    }

    
}
