<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\HelperFunctions_add_rt;

class AdminControllerPerspective extends Controller
{
    //
    
    //    -------------ЗК, перспективные -------------------
    //отображение начальной страницы добавления радиотехнологии. ЗК, перспективные
    public function show_zk(Request $request) {
//        return view('add_rt', ['user_type' => 1, 'rt_active_perspect_type' => 0]);
        
        $user_type = 1;
        $rt_active_perspect_type = 0;

        return HelperFunctions_add_rt::show_add_rt_page_common($request, $user_type, $rt_active_perspect_type);
        
    }
    
    public function change_zk(Request $request) {
        $user_type = 1;
        $rt_active_perspect_type = 0;

        return HelperFunctions_add_rt::change_db_row_common($request, $user_type, $rt_active_perspect_type);
    }
    
    public function show_sk(Request $request) {
//        return view('add_rt', ['user_type' => 0, 'rt_active_perspect_type' => 0]);
        
        $user_type = 0;
        $rt_active_perspect_type = 0;

        return HelperFunctions_add_rt::show_add_rt_page_common($request, $user_type, $rt_active_perspect_type);
    }
    
    public function change_sk(Request $request) {
        $user_type = 0;
        $rt_active_perspect_type = 0;

        return HelperFunctions_add_rt::change_db_row_common($request, $user_type, $rt_active_perspect_type);
    }
}
