<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\rfplan_row;
use App\rt_freq_value;

use Validator;

use App\Helpers\HelperFunctions_add_rt;

class AdminControllerActive extends Controller {

//    -------------ЗК, активные-------------------
    //отображение начальной страницы добавления радиотехнологии. ЗК, активные
    public function show_zk(Request $request) {

        // начальная загрузка страницы - отобразить первую радиотехнологию
  
        $user_type = 1;
        $rt_active_perspect_type = 1;

//        return $this->show_add_rt_page_common($request, $user_type, $rt_active_perspect_type);
        return HelperFunctions_add_rt::show_add_rt_page_common($request, $user_type, $rt_active_perspect_type);
    }

    //внесение изменений (добавление, удаление, обновление) в План частот 
    //для радиотехнологии. ЗК, активные
    public function change_zk(Request $request) {

        $user_type = 1;
        $rt_active_perspect_type = 1;

        return HelperFunctions_add_rt::change_db_row_common($request, $user_type, $rt_active_perspect_type);
    }

    
    
    
//    -------------СК, активные-------------------
    //отображение начальной страницы добавления радиотехнологии. СК, активные
    public function show_sk(Request $request) {
        // начальная загрузка страницы - отобразить первую радиотехнологию

        $user_type = 0;
        $rt_active_perspect_type = 1;

        return HelperFunctions_add_rt::show_add_rt_page_common($request, $user_type, $rt_active_perspect_type);
    }

    //внесение изменений (добавление, удаление, обновление) в План частот 
    //для радиотехнологии. СК, активные
    public function change_sk(Request $request) {
        
        $user_type = 0;
        $rt_active_perspect_type = 1;

        return HelperFunctions_add_rt::change_db_row_common($request, $user_type, $rt_active_perspect_type);
    }

    
}
