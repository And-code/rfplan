<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use App\rfplan_release_description;
use App\Helpers\HelperFunctions;

class AdminControllerDbManagement extends Controller
{
    

    //отображение страницы редактирования таблиц базы данных Плана частот.
    public function show(Request $request) {
                
        $descriptions = \App\rfplan_release_description::where('actual', true)->get()->all();
        
        $message = "OK";
        $type = "success";
        return view('db_manag', ['message' => $message,
                                    'type' => $type,
                                    'descriptions' => $descriptions]);
    }

    //внесение изменений в базу данных - добавление/удаление/редактирование таблиц
    public function change(Request $request) {
        
       $request_data = $request->except("_token");
        
        $action_type = $request_data['action_type'];
        $table_plan = $request_data['db_name'];
        
        
          if ($action_type == "delete")  {
                    
                    $db_mange_result_JSON = \App\Helpers\HelperFunctions_db_manag::table_delete($request_data);

                    return $db_mange_result_JSON; // возвращаем для JSON запроса
                
                
        } else if ($action_type == "update")  {        
            
            $db_mange_result_JSON = \App\Helpers\HelperFunctions_db_manag::table_update($request_data);
            
            return $db_mange_result_JSON; // возвращаем для JSON запроса
            
        } else if ($action_type == "clone")  {
              
            $db_mange_result_JSON = \App\Helpers\HelperFunctions_db_manag::table_clone($request_data);

            return $db_mange_result_JSON; // возвращаем для JSON запроса
                
        } else if ($action_type == "create")  {
              
            $db_mange_result_JSON = \App\Helpers\HelperFunctions_db_manag::table_create($request_data);
            
            return $db_mange_result_JSON; // возвращаем для JSON запроса
                
        } else if ($action_type == "select")  {

            $db_mange_result_JSON = \App\Helpers\HelperFunctions_db_manag::table_select($request_data);

            return $db_mange_result_JSON; // возвращаем для JSON запроса
                    
        }

        return;
    }
    
}
