<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    
    public function register(Request $request) {
//        parent::register($request);
        
        $this->validator($request->all())->validate();

        
        // если первая регистрация, то логинимся и переходим к 
        // редактированию конкретной таблицы
        if (User::all()->isEmpty()) {
            event(new Registered($user = $this->create($request->all())));
            $this->guard()->login($user);

            return $this->registered($request, $user)
                        ?: redirect(route('show_rt_zk_active', app()->getLocale()));
            
        } else {
            event(new Registered($user = $this->create($request->all())));
                        
            return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
        }
                
    }
    
    
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = '/home';
//    protected $redirectTo = app()->getLocale() . '/admin/add_rt_zk_active';

    public function redirectTo()
    {
//        return app()->getLocale() . '/admin/add_rt_zk_active';
//        return route('show_db_manag_page', app()->getLocale());
        
        session()->flash('status', __('Реєстрація нового користувача успішна!'));
        
        return route('register', app()->getLocale());
    }
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
        
        if (User::all()->isEmpty()) {
            $this->middleware('guest');
        } else {
            $this->middleware('auth');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'unique:users'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
//            'email' => $data['email'],
            'email' => "",
            'password' => Hash::make($data['password']),
        ]);
    }
    
    
    
}
