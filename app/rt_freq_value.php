<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rt_freq_value extends Model
{
    //
    protected $table = "rt_freq_values";
    
    // поля в таблице, которые разрешается изменять
    protected $fillable = ['entry_id', 'rf_start_mhz', 'rf_stop_mhz'];
    
       public function __construct($type = null) {

        parent::__construct();

        $this->setTable($type);
    }
    
     public function rfplan_row() {
        return $this->belongsTo("App\rfplan_row", 'entry_id', "id");
    }
}
