<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use DB;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
//        DB::listen(function ($query) {
//            
//            dump($query->sql);
////            dump($query->bindings);
//            
//        });
        
//        Schema::defaultStringLength(191);
        
        Paginator::defaultView('add_rt_layouts.navbar_paginate_add_rt');
        
    }
}
