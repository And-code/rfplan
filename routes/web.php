<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'{locale}', 
              'where' => ['locale' => '[a-zA-Z]{2}'],
              'middleware' => 'setlocale'], function($locale){
    
    // стандартные маршруты аутентификации без сброса пароля и проверки email
    Auth::routes(['reset' => false, 'verify' => false, ]);
    
    Route::get('/', ["uses"=>"MainPageController@show", "as"=>"main"]);
    Route::post('/', ["uses"=>"MainPageController@showSearch", "as"=>"mainPost"]);

    

    
    Route::group(['prefix'=>'/admin', 'middleware'=>['auth']], function(){
        Route::get('/add_rt_zk_active', ['uses'=>'Admin\AdminControllerActive@show_zk', 'as'=>'show_rt_zk_active']);
        Route::post('/add_rt_zk_active', ['uses'=>'Admin\AdminControllerActive@change_zk', 'as'=>'change_rt_zk_active']);

        Route::get('/add_rt_sk_active', ['uses'=>'Admin\AdminControllerActive@show_sk', 'as'=>'show_rt_sk_active']);
        Route::post('/add_rt_sk_active', ['uses'=>'Admin\AdminControllerActive@change_sk', 'as'=>'change_rt_sk_active']);

        Route::get('/add_rt_zk_perspective', ['uses'=>'Admin\AdminControllerPerspective@show_zk', 'as'=>'show_rt_zk_perspective']);
        Route::post('/add_rt_zk_perspective', ['uses'=>'Admin\AdminControllerPerspective@change_zk', 'as'=>'change_rt_zk_perspective']);

        Route::get('/add_rt_sk_perspective', ['uses'=>'Admin\AdminControllerPerspective@show_sk', 'as'=>'show_rt_sk_perspective']);
        Route::post('/add_rt_sk_perspective', ['uses'=>'Admin\AdminControllerPerspective@change_sk', 'as'=>'change_rt_sk_perspective']);

        Route::get('/db_manag', ['uses'=>'Admin\AdminControllerDbManagement@show', 'as'=>'show_db_manag_page']);
        Route::post('/db_manag', ['uses'=>'Admin\AdminControllerDbManagement@change', 'as'=>'change_db_manag_page']);
    
    });
});

Route::get('/', function(){
    return redirect(app()->getLocale());
});


