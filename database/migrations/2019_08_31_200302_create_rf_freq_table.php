<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfFreqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rt_freq_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("entry_id")->unsigned();
            $table->decimal("rf_start_mhz", 13, 6)->default(0);
            $table->decimal("rf_stop_mhz", 13, 6)->default(0);
                    
            
            $table->foreign('entry_id')
      ->references('id')->on('rfplan_rows')
      ->onDelete('cascade')->onUpdate('cascade');
                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rt_freq_values');
    }
}
