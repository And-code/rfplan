<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfplan_release_descriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("rfplan_table_name", 255)->nullable()->collation("utf8mb4_unicode_ci");
            
            $table->text("alias")->nullable()->default(null)->collation("utf8mb4_unicode_ci");
            
//            $table->text("rfplan_date")->nullable()->default(NULL)->collation("utf8mb4_unicode_ci");
//            $table->text("plan_release_descr")->nullable()->default(NULL)->collation("utf8mb4_unicode_ci");
            $table->text("rfplan_date")->nullable()->collation("utf8mb4_unicode_ci");
            
            $table->text("plan_release_descr")->nullable()->collation("utf8mb4_unicode_ci");
            $table->boolean("actual")->default(true);
            $table->timestamps();
            
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rfplan_release_descriptions');
    }
}
