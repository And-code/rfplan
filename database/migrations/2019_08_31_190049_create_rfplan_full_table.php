<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfplanFullTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfplan_rows', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->decimal("rt_numb", 6, 3);
            
            $table->mediumInteger("freq_subgroup_numb"); // 3 bytes
            
            $table->text("rt_name")->collation("utf8mb4_unicode_ci");
            $table->text("rs_name")->collation("utf8mb4_unicode_ci");
            $table->text("rcom_type")->collation("utf8mb4_unicode_ci");
            $table->text("std_base")->collation("utf8mb4_unicode_ci");
            $table->text("std_gen")->collation("utf8mb4_unicode_ci");
            $table->text("other_norms")->collation("utf8mb4_unicode_ci");
            $table->text("rt_freq_all")->collation("utf8mb4_unicode_ci");
            $table->text("rt_use_features")->collation("utf8mb4_unicode_ci");
            $table->text("rt_usestop_date")->collation("utf8mb4_unicode_ci");
            $table->text("rt_persp_usestart_date")->collation("utf8mb4_unicode_ci");
            $table->text("rt_notes")->collation("utf8mb4_unicode_ci");
            
            $table->date("gov_accept_date")->nullable();
            
            $table->tinyInteger("user_type")->default(1);
            $table->tinyInteger("rt_active_perspect_type")->default(1);
            
            $table->timestamps();
            
            $name = 'rt_numb';
            $columns = [DB::raw("`rt_numb`, `rt_name`(100), 
                `rs_name`(20), `rcom_type`(20), 
                `std_base`(20), `std_gen`(20), 
                `other_norms`(20), `rt_freq_all`(20), 
                `rt_use_features`(20), `rt_notes`(20) , 
                `user_type`, `rt_active_perspect_type`")];
            
            $table->unique($columns, $name);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rfplan_rows');
    }
}
