
<div class="d-flex flex-column pr-2"> 
<div class="custom-control custom-checkbox">
    <input type="checkbox" 
           class="custom-control-input" 
           id="ZK_active"           
           onclick=" update_rt_list_by_checkbox('ZK_active', 'class11', true);"
           value="ZK_active"
           checked >
    <label class="custom-control-label" for="ZK_active">{{ __('ЗК, діючі') }}</label>
</div>
<div class="custom-control custom-checkbox">
    <input type="checkbox" 
           class="custom-control-input" 
           id="SK_active"
           onclick=" update_rt_list_by_checkbox('SK_active', 'class01', true);"
           value="SK_active"
           checked >
    <label class="custom-control-label" for="SK_active">{{ __('СК, діючі') }}</label>
</div>
</div>

<div class="d-flex flex-column pr-2"> 

<div class="custom-control custom-checkbox">
    <input type="checkbox" 
           class="custom-control-input" 
           id="ZK_perspective"
           onclick=" update_rt_list_by_checkbox('ZK_perspective', 'class10', true);"
           value="ZK_perspective"
           checked >
    <label class="custom-control-label" for="ZK_perspective">{{ __('ЗК, перспективні') }}</label>
</div>
<div class="custom-control custom-checkbox">
    <input type="checkbox" 
           class="custom-control-input" 
           id="SK_perspective"
           onclick=" update_rt_list_by_checkbox('SK_perspective', 'class00', true);"
           value="SK_perspective"
           checked >
    <label class="custom-control-label" for="SK_perspective">{{ __('СК, перспективні') }}</label>
</div>
</div>


