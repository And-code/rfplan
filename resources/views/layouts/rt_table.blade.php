
@if ($rt_rows_zk_active != null)
    @include('layouts.rt_active_table', ['table_rows' => $rt_rows_zk_active, 'user_type' => 1, 'table_style_class' => 'zk_table_style', 'table_style_class_th_td' => 'zk_table_style_th_td', 'table_style_class_tr' => 'zk_table_style_tr'])
@endif

@if ($rt_rows_sk_active != null)
    @include('layouts.rt_active_table', ['table_rows' => $rt_rows_sk_active, 'user_type' => 0, 'table_style_class' => 'sk_table_style', 'table_style_class_th_td' => 'sk_table_style_th_td', 'table_style_class_tr' => 'sk_table_style_tr'])
@endif

@if ($rt_rows_zk_perspective != null)
    @include('layouts.rt_perspective_table', ['table_rows' => $rt_rows_zk_perspective, 'user_type' => 1, 'table_style_class' => 'zk_table_style', 'table_style_class_th_td' => 'zk_table_style_th_td', 'table_style_class_tr' => 'zk_table_style_tr'])
@endif

@if ($rt_rows_sk_perspective != null)
    @include('layouts.rt_perspective_table', ['table_rows' => $rt_rows_sk_perspective, 'user_type' => 0, 'table_style_class' => 'sk_table_style', 'table_style_class_th_td' => 'sk_table_style_th_td', 'table_style_class_tr' => 'sk_table_style_tr'])
@endif

