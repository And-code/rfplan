

<!--            <form class="row sticky-top" id="search_form" action="{ { route('mainPost') }}" method="POST">

                @csrf-->

<div class="row sticky-top" id="search_form">

    <div class="col">

        <!--<div class="col-sm-8" id="search_main">-->

        <div class="row" id="search_main"> 

            <!--div с поиском по Плану частот--> 
            <div class="col-md-5"> 

                <div class="input-group"> 
                    <input type="text" class="form-control" 
                           name="search_text" 
                           id="search_text" 
                           placeholder="{{ __('Пошуковий запит...') }}" 
                           value=""
                           
                           
                           >

                    <!--< ?php // echo "value=\"" . (!empty($_POST) ? $_POST['search_text'] . "\"" : "\"") ?> >--> 

                    <div class="input-group-append"> 
                        <!--<input type="submit" class="btn btn-info" value="Пошук">-->   
                        <button id="submit" class="btn btn-info">{{ __('Пошук') }}</button> 
                    </div>
                </div>

            </div>
            <!--div с редактирование Плана частот--> 
            <div class="col-md-2"> 
                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#collapsableFilters">
                    {{ __('Уточнити пошук') }}
                </button>

            </div>

            <!--div с редактирование Плана частот--> 
            <div class="col-md-3"> 
                <!--реализовать тут авторизацию и проверку авторизации-->

<!--<span id="button_add_rt"> <a class="btn btn-info" href="./add_rt_page_2_zk_diuchi_part.php">Редагування Плану частот </a> </span>-->
                <span id="button_add_rt"> <a class="btn btn-info w-full" href="{{ route('show_rt_zk_active', app()->getLocale()) }}">{{ __('Редагування Плану частот') }}</a> </span>
                @guest
                    
                @else

                    <span class="text-danger col-md-2"><br>{{ __('В режимі адміна!') }}</span>
                @endguest
                
            </div>
             
            
            <!--<div class="col-md-3 ml-auto">-->
            <div class="col-md-1 mr-auto">
                
                <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout', app()->getLocale()) }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Вихід') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                
                
            </div>
            
            @section('locale')
                @include('layouts.div_locale_switch')
            @show
            

        </div>

        <!--</div>-->

        <div class="row collapse" id="collapsableFilters"> 

            <div class="col-sm-9 mt-1" >
                <!--Блок выбора частот для поиска-->
                <!--Ниже будет появляться код отображения выбора частот-->
                <span><strong>{{ __('Обмежити за частотою:') }}</strong></span>
                <div class="row">

                    @section('frequencies_constraints')

                        <!--@ include('layouts.frequencies_fields')-->
                        @include('add_rt_layouts.freq_fields_html', ['frequencies' => null, 'set_div_id' => true])

                    @show


                </div>           

                <!--Выпадающий список со списком радиотехнологий-->
                <div class="row"> 
                    <div class="col" id="select_rt_div">

                        @section('rt_list')
                            @include('layouts.rt_list')
                        @show
                        
                    </div>
                </div>

            </div>

            <!--Блок выбора типа пользователей: загальни, специальни користувачи. перспективные или действующие-->
            <div class="col-sm-3 mt-1" id="search_checkboxs">

                @section('checkboxes')
                    @include('layouts.checkboxes')
                @show


            </div>
        </div>



    </div>
</div> <!-- end of search div -->
<!--</form>-->

