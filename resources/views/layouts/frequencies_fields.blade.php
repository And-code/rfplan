


<div class="col my-1" id="rfequencies_fields">

    
    
    <div class="input-group" id="www">

        <div class="input-group-prepend">
            <span class="input-group-text">від</span>
        </div>

        <input class="form-control" 
               type="text" 
               name="rf_start_mhz" 
               id="rf_start_mhz" 
               value="0"
               onClick="this.setSelectionRange(0, this.value.length)"
               >

        
        
        <select class="btn btn-info" name="freq_from_unit" id="freq_from_unit">
                <option value="Гц" >Гц</option>
                <option value="кГц" >кГц</option>
                <option value="МГц" selected >МГц</option>
                <option value="ГГц" >ГГц</option>
        </select>

        <div class="input-group-prepend">
            <span class="input-group-text">до</span>
        </div>

        <input class="form-control" 
               type="text" 
               name="rf_stop_mhz" 
               id="rf_stop_mhz" 
               value="0"
               onClick="this.setSelectionRange(0, this.value.length)">

        <select class="btn btn-info" name="freq_to_unit" id="freq_to_unit">
            <option value="Гц" >Гц</option>
            <option value="кГц" >кГц</option>
            <option value="МГц" selected >МГц</option>
            <option value="ГГц" >ГГц</option>
        </select>

        <input type="button" 
               class="btn btn-info" 
               name="clear_freq" 
               value="Очистити" 
               onclick="clear_freq_fields_numbered(0); search_onchange();">
    </div>
</div>
