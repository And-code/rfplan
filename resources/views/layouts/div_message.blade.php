
{{-- функция формирования html с div с сообщением про ошибку или успех --}}
    {{-- $type - 'success', 'warning', 'danger' --}}
     

        <div class="row justify-content-center" id="id_div_messages">  
             <div class="col-auto"> 

                    
                 @if ( $type == 'warning')
                     <div class="{{ 'alert text-center alert-warning' }}">
                         <strong>{{ __('Увага!') }} </strong>{!! $message !!}
                 @elseif ( $type == 'danger' )
                     <div class="{{ 'alert text-center alert-danger' }}">
                         <strong>{{ __('Помилка!') }} </strong>{!! $message !!}
                 @else 
                     <div class="{{ 'alert text-center alert-success' }}">
                         <strong>{{ __('Успіх!') }} </strong>{!! $message !!}
                 @endif
                 
                </div>
            </div>
        </div>
    