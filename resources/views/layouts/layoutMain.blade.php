<!DOCTYPE html>
<!--
RF plan, Ukraine.
Website for view and search in RF plan. Laravel
-->


<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{{ __('План частот України') }}</title>
<!--                <link rel="stylesheet" href="./style/style_rf_plan.css">    
                <link rel="stylesheet" href="./style/modal_clone.css">    
                <link rel="stylesheet" href="./style/style_add_rt.css">    -->
                <link rel="stylesheet" href="{{ asset('css/style_rf_plan.css') }}">    
                <link rel="stylesheet" href="{{ asset('css/modal_clone.css') }}">    
                <link rel="stylesheet" href="{{ asset('css/style_add_rt.css') }}">    
        
                


        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- provide the csrf token -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

                
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 

        
        <script src="{{ asset('js/js_functions_common.js') }}"></script>
                <script src="{{ asset('js/update_rt_select_list_new.js') }}"></script>
                <script src="{{ asset('js/search_func.js') }}"></script>

    </head>
    <body>

        
        <div class="container-fluid"> 

            
            @section('title')
            <div class="row"> 
                <div class="col"> 
                    <h1 class="my-title" style="text-align: center"><a href="{{ route('main', app()->getLocale()) }}">
                            {{ __('План використання радіочастотного ресурсу України') }}
                            </a></h1>
                    <h4 class="my-subtitle" style="text-align: center; fo">{{ __('Затверджено постановою Кабінету Міністрів України від 9 червня 2006 р. № 815') }}</h4>

                </div>

            </div>
            @show
            
            @section('form')
            
                @include('layouts.search_form')
            
            @show
            
            @section('select_rf_plan_date')
            
                @include('layouts.div_select_rf_plan_date')
            
            @show
                       
            
            <!--Блок вывода таблицы Плана частот-->
            <div class="row"> 

                <div class="col" id="rf_plan_table">

                    @section('rt_table')
                    
                        @include('layouts.rt_table')
                    
                    @show

                </div>
            </div>
            
            
        </div>
        
        
        
        
    </body>
</html>