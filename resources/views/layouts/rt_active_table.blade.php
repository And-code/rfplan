
<!--таблица активных радиотехнологий-->

<table class='{{ $table_style_class }} table table-sm table-bordered table-striped table-hover'>
    <thead>
    <th class='{{ $table_style_class_th_td }}'>№</th>
    <th class='{{ $table_style_class_th_td }}'>Радіотехнологія</th>
    <th class='{{ $table_style_class_th_td }}'>Радіослужба</th>
    <th class='{{ $table_style_class_th_td }}'>Вид радіозв'язку</th>
    <th class='{{ $table_style_class_th_td }}'>Базові стандарти</th>
    <th class='{{ $table_style_class_th_td }}'>Основні загальні стандарти</th>
    <th class='{{ $table_style_class_th_td }}'>Положення РР МСЕ, резолюції ВКР, рекомендації МСЕ, СЕПТ, рішення ЄКК, міжнародні угоди, акти законодавства ЄС</th>
    <th class='{{ $table_style_class_th_td }}'>Смуга радіочастот</th>
    <th class='{{ $table_style_class_th_td }}'>Особливості застосування радіотехнологій</th>
    <th class='{{ $table_style_class_th_td }}'>Строк припинення використання радіотехнології</th>
    </thead>
<!--// --- Заголовок таблицы -- КОНЕЦ------>
<!--// Название раздела-->
<tr>
    <th class='{{ $table_style_class_th_td }}' colspan="10" >


        @if ($user_type == 1) 
            Радіотехнології, які застосовуються загальними користувачами
        @else
            Радіотехнології, які застосовуються спеціальними користувачами
        @endif

    </th>
</tr>

@if ($table_rows->count() == 0) 

    <tr>
        <td class='{{ $table_style_class_tr }}' colspan="10" >

            --Пусто...--

        </td>
    </tr> <br>

@else

    <!-- --- тело таблицы------->
    @foreach ($table_rows as $row)

        <tr>
            <td class='{{ $table_style_class_tr }}'>
                {!! nl2br($row->rt_numb) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                {!! nl2br($row->rt_name) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->rs_name). "&nbsp")-->
                {!! nl2br($row->rs_name) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->rcom_type). "&nbsp")-->
                {!! nl2br($row->rcom_type)  !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->std_base). "&nbsp")-->
                {!! nl2br($row->std_base)  !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->std_gen). "&nbsp")-->
                {!! nl2br($row->std_gen)  !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->other_norms). "&nbsp")-->
                {!! nl2br($row->other_norms) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->rt_freq_all). "&nbsp")-->
                {!! nl2br($row->rt_freq_all) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->rt_use_features). "&nbsp")-->
                {!! nl2br($row->rt_use_features) !!} &nbsp 
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->rt_usestop_date). "&nbsp")-->
                {!! nl2br($row->rt_usestop_date) !!} &nbsp
            </td>



         <!--строка с "примитками"-->
        @if ($row->rt_notes != "")
            </tr>
            <tr>
                <td class='{{ $table_style_class_tr }}' colspan="10">
                    {!! nl2br($row->rt_notes) !!} 
                    <!--print nl2br($row['rt_notes']);-->
                </td>
            </tr>
        @else
            </tr>
        @endif
            
    @endforeach

@endif

<!----- тело таблицы--КОНЕЦ----->
<br>
