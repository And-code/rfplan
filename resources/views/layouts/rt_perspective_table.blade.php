
<!--таблица перспективных радиотехнологий-->

<table class='{{ $table_style_class }} table table-sm table-bordered table-striped table-hover'>
    <thead>
    <th class='{{ $table_style_class_th_td }}'>№</th>
    <th class='{{ $table_style_class_th_td }}'>Назва радіотехнології</th>
    <th class='{{ $table_style_class_th_td }}'>Базові стандарти</th>
    <th class='{{ $table_style_class_th_td }}'>Радіослужба, якою планується використання радіотехнології</th>
    <th class='{{ $table_style_class_th_td }}'>Смуга радіочастот</th>
    <th class='{{ $table_style_class_th_td }}'>Особливості впровадження радіотехнологій</th>
    <th class='{{ $table_style_class_th_td }}'>Початок впровадження</th>
</thead>
<!--// --- Заголовок таблицы -- КОНЕЦ------>
<!--// Название раздела-->
<tr>
    <th class='{{ $table_style_class_th_td }}' colspan="7" >

        @if ($user_type == 1) 
            Радіотехнології, які плануються для застосування загальними користувачами
        @else
            Радіотехнології, які плануються для застосування спеціальними користувачами
        @endif

    </th>
</tr>

@if ($table_rows->count() == 0) 

    <tr>
        <td class='{{ $table_style_class_tr }}' colspan="7" >

            --Пусто...--

        </td>
    </tr> <br>

@else

    <!-- --- тело таблицы------->
    @foreach ($table_rows as $row)

        <tr>
            <td class='{{ $table_style_class_tr }}'>
                {!! nl2br($row->rt_numb) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                {!! nl2br($row->rt_name) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->rs_name))-->
                {!! nl2br($row->std_base) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->rs_name))-->
                {!! nl2br($row->rs_name) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->rcom_type))-->
                {!! nl2br($row->rt_freq_all) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->std_base))-->
                {!! nl2br($row->rt_use_features) !!} &nbsp
            </td>

            <td class='{{ $table_style_class_tr }}'>
                <!--nl2br(($row->std_gen))-->
                {!! nl2br($row->rt_persp_usestart_date) !!} &nbsp
            </td>


         <!--строка с "примитками"-->
        @if ($row->rt_notes != "")
            </tr>
            <tr>
                <td class='{{ $table_style_class_tr }}' colspan="7">
                    {!! nl2br($row->rt_notes) !!} 
                    <!--print nl2br($row['rt_notes']);-->
                </td>
            </tr>
        @else
            </tr>
        @endif
        
    @endforeach

@endif

<!----- тело таблицы--КОНЕЦ----->
<br>
