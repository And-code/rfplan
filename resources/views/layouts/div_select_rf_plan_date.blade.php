
            <div class="row"> 

                <div class="col"> 

                    <div class="row" id="select_db_div"> 

                        <div class="col-auto my-auto" style="background-color: lightgray">
                            <span><strong>{{ __('Вибір Плану частот за датою редакції:') }}</strong></span>

                        </div>

                        <div class="col-auto" style="background-color: lightgray">
                            
                            
                            
                            <!-- --- тело списка------->

                            <!--<select class= "custom-select" name="db_choice" id="db_choice" onchange="change_db(this.value)">-->
                            <select class= "custom-select" name="db_choice" id="db_choice" onchange="search_onchange()">

                            
                            @forelse ($rf_plan_list as $key => $value) 

                                @if ($value->rfplan_table_name != session('rfplan_rows')) 
                                    <option value="{{$key}}">{{ $value->rfplan_date }}</option>

                                @else
                                    <option value="{{$key}}" selected>{{ $value->rfplan_date }}</option>

                                @endif
                                
                            @empty
                            
                                <option value="0" selected>{{ __('Пусто')}}</option>
                                
                            @endforelse
                            
                            </select>
                            
                            
                        </div>
                        
                    </div>



                    <p style="text-align: center; font-weight: bold" 
                       id="span_db_descr_id">

                        @if ( empty($rf_plan_list->firstWhere('rfplan_table_name', session('rfplan_rows'))) )
                            {{ __('Пусто') }}
                        @else
                            {!! nl2br($rf_plan_list->firstWhere('rfplan_table_name', session('rfplan_rows'))->plan_release_descr) !!}
                        @endif
                        

                    </p>

                </div>

            </div>