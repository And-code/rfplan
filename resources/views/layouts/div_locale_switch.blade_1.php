<div class="col-md-1 container-fluid">

    <!-- Right Side Of Navbar -->
    <!-- ссылки переключения локалей -->
    <nav class="navbar navbar-expand-md"> 
        <ul class="navbar-nav">
            @foreach( config('app.available_locales') as $locale)
            <li class="nav-item">
                <a class="nav-link text-white btn"
                   href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), $locale)}}"
                   @if (app()->getLocale() == $locale)
                   style="font-weight: bold; text-decoration: underline"
                   @endif
                   >{{ strtoupper($locale) }}</a>
            </li>
            @endforeach
        </ul>
    </nav>

</div>