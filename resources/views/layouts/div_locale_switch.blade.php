<!--<div class="col-sm-3">-->
<div class="col-auto d-inline-flex m-auto">

    
    <div class="btn-group ml-auto"> 
    
    
    
    @foreach( config('app.available_locales') as $locale)
            
                <a class="btn text-white"
                   href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), $locale)}}"
                   @if (app()->getLocale() == $locale)
                    style="font-weight: bold; text-decoration: underline"
                   @endif
                   >{{ strtoupper($locale) }}</a>
            
            @endforeach
    
    </div>


</div>