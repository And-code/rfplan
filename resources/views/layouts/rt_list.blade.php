

<span class="mt-1"><strong>{{ __('Вибрати радіотехнологію:') }} </strong></span>
<select class="custom-select" name="rt_choice" id="rt_choice" onchange="search_onchange();">
    <option value="0">{{ __('---Усі радіотехнології---') }}</option>
    
    @foreach($rt_list_arrays as $rt_list_array)
    
        @if ($rt_list_array->user_type == 1)
            @if ($rt_list_array->rt_active_perspect_type == 1)
                <option value="{{ $rt_list_array->id }}" class="class11">ЗК | {{ $rt_list_array->rt_numb . " | " . $rt_list_array->rt_name }}</option>
            @else
                <option value="{{ $rt_list_array->id }}" class="class10">ЗК | {{ $rt_list_array->rt_numb . " | " . $rt_list_array->rt_name }}</option>
            @endif
        @else
            @if ($rt_list_array->rt_active_perspect_type == 1)
                <option value="{{ $rt_list_array->id }}" class="class01">СК | {{ $rt_list_array->rt_numb . " | " . $rt_list_array->rt_name }}</option>
            @else
                <option value="{{ $rt_list_array->id }}" class="class00">CК | {{ $rt_list_array->rt_numb . " | " . $rt_list_array->rt_name }}</option>
            @endif
        @endif
        
    @endforeach
    
</select>

