

@if(empty($set_div_id))
    <div class="col my-1">
@else
    <div class="col my-1" id="rfequencies_fields">
@endif

        <div class="row" id="{{ $childId }}">

            <div class="col-lg pt-1 input-group"> 
                <div class="input-group-prepend">
                    <span class="input-group-text" id="{{ "from". $key }}">{{ __('від') }}</span>
                </div>

                <input class="form-control" 
                       type="text" 
                       name="{{ 'rf_start_mhz' . $key }}" 
                       id="{{ 'rf_start_mhz' . $key }}" 
                       value="{{ $row['rf_start_mhz'] }}"
                       onClick="this.setSelectionRange(0, this.value.length)"
                       >

                <select class="btn btn-info" name="{{ "freq_from_unit" . $key }}" id="{{ "freq_from_unit" . $key }}">
                    <option value="Гц" >Гц</option>
                    <option value="кГц" >кГц</option>
                    <option value="МГц" selected >МГц</option>
                    <option value="ГГц" >ГГц</option>
                </select>
            </div>

            <!--<div class="col-lg pt-1 input-group d-flex flex-column flex-lg-row">--> 
            <div class="col-lg pt-1 input-group"> 

                
                <div class="input-group-prepend">
                    <span class="input-group-text" id="{{ "to". $key }}">{{ __('до') }}</span>
                </div>

                <input class="form-control" 
                       type="text" 
                       name="{{ 'rf_stop_mhz' . $key }}" 
                       id="{{ 'rf_stop_mhz' . $key }}"
                       value="{{ $row['rf_stop_mhz'] }}"
                       onClick="this.setSelectionRange(0, this.value.length)">

                <select class="btn btn-info" name="{{ "freq_to_unit" . $key }}" id="{{ "freq_to_unit" . $key }}">
                    <option value="Гц" >Гц</option>
                    <option value="кГц" >кГц</option>
                    <option value="МГц" selected >МГц</option>
                    <option value="ГГц" >ГГц</option>
                </select>
                
                
<!--                    <input type="button" 
                       class="btn btn-info" 
                       name="{{ "clear_freq" . $key }}"  
                       id="{{ "clear_freq" . $key }}"  
                       value="{{ __('Очистити') }}" 
                       onclick="clear_freq_fields_numbered({{  $key }})">
                
                


                @if ($key != 0)
                <span class="btn btn-info" onclick="var child = document.getElementById({{ '\'' . $childId . '\'' }}); child.parentNode.removeChild(child);">{{ __('Видалити') }}</span>
                @else 
                <span class="d-none" id="{{ "delete_freq_btn" . $key }}" >{{ __('Видалити') }}</span>
                @endif-->
            </div>

            <!--<div class="col-lg pt-1 d-flex flex-column flex-lg-row">-->
            <div class="col-lg-auto pt-1">
                
                <input type="button" 
                       class="btn btn-info" 
                       name="{{ "clear_freq" . $key }}"  
                       id="{{ "clear_freq" . $key }}"  
                       value="{{ __('Очистити') }}" 
                       onclick="clear_freq_fields_numbered({{  $key }})">
                
                


                @if ($key != 0)
                <span class="btn btn-info" onclick="var child = document.getElementById({{ '\'' . $childId . '\'' }}); child.parentNode.removeChild(child);">{{ __('Видалити') }}</span>
                @else 
                <!--<span class="btn invisible" id="{ { "delete_freq_btn" . $ key }}" >{ { __('Видалити') }}</span>-->
                <span class="btn invisible" id="{{ "delete_freq_btn" . $key }}" >{{ __('Видалити') }}</span>
                @endif
                
            </div>



        </div>
    </div>