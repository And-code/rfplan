
@if ($user_type == 1 && $rt_active_perspect_type == 1) 
    <h4 class="rt_title">{{ __('Радіотехнології, які застосовуються загальними користувачами (діючі)') }}</h4>

@elseif ($user_type == 0 && $rt_active_perspect_type == 1) 
    <h4 class="rt_title">{{ __('Радіотехнології, які застосовуються спеціальними користувачами (діючі)') }}</h4>

@elseif ($user_type == 1 && $rt_active_perspect_type == 0) 
    <h4 class="rt_title">{{ __('Радіотехнології, які застосовуються загальними користувачами (перспективні)') }}</h4>

@elseif  ($user_type == 0 && $rt_active_perspect_type == 0) 
    <h4 class="rt_title">{{ __('Радіотехнології, які застосовуються спеціальними користувачами (перспективні)') }}</h4>

@endif



<div>
    <p><span class="error">{{ __('* обовʼязкові поля') }}</span></p>

    <form id="qqq" action="{{ route('change_rt_zk_active', app()->getLocale()) }}" method="post">


<!--// если действующие радиотехнологии-->
@if ($rt_active_perspect_type == 1) 

    @include('add_rt_layouts.add_rt_divs.div_get_rt_list')
    @include('add_rt_layouts.add_rt_divs.div_id_of_entry')
    @include('add_rt_layouts.add_rt_divs.div_rt_numb')
    @include('add_rt_layouts.add_rt_divs.div_freq_subgroup_numb')
    @include('add_rt_layouts.add_rt_divs.div_rt_name')
    @include('add_rt_layouts.add_rt_divs.div_rs_name')
    @include('add_rt_layouts.add_rt_divs.div_rcom_type')
    @include('add_rt_layouts.add_rt_divs.div_std_base')
    @include('add_rt_layouts.add_rt_divs.div_std_gen')
    @include('add_rt_layouts.add_rt_divs.div_other_norms')
    @include('add_rt_layouts.add_rt_divs.div_rt_freq_all')
    @include('add_rt_layouts.add_rt_divs.div_rf_start_stop_mhz', ['frequencies' => $frequencies==null ? null : $frequencies->all()] )
    @include('add_rt_layouts.add_rt_divs.div_rt_use_features')
    @include('add_rt_layouts.add_rt_divs.div_rt_usestop_date')
    @include('add_rt_layouts.add_rt_divs.div_rt_notes')
    @include('add_rt_layouts.add_rt_divs.div_gov_accept_date')

@elseif ($rt_active_perspect_type == 0) 
    <!--// если перспективные радиотехнологии-->

    @include('add_rt_layouts.add_rt_divs.div_get_rt_list')
    @include('add_rt_layouts.add_rt_divs.div_id_of_entry')
    @include('add_rt_layouts.add_rt_divs.div_rt_numb')
    @include('add_rt_layouts.add_rt_divs.div_freq_subgroup_numb')
    @include('add_rt_layouts.add_rt_divs.div_rt_name')
    @include('add_rt_layouts.add_rt_divs.div_std_base')
    @include('add_rt_layouts.add_rt_divs.div_rs_name')
    @include('add_rt_layouts.add_rt_divs.div_rt_freq_all')
    @include('add_rt_layouts.add_rt_divs.div_rf_start_stop_mhz', ['frequencies' => $frequencies==null ? null : $frequencies->all()])
    @include('add_rt_layouts.add_rt_divs.div_rt_use_features')
    @include('add_rt_layouts.add_rt_divs.div_rt_persp_usestart_date')
    @include('add_rt_layouts.add_rt_divs.div_rt_notes')
    @include('add_rt_layouts.add_rt_divs.div_gov_accept_date')

@endif



    </form>

</div>

