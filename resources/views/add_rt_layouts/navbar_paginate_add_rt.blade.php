
<!--Навигация по спискам-->
<!--<nav class="navbar navbar-expand-sm bg-secondary navbar-dark text-center sticky-top" id="navbar">--> 
<!--<nav class="navbar navbar-expand-sm bg-secondary navbar-dark text-center sticky-top" id="navbar">--> 
<nav class="navbar navbar-expand bg-secondary navbar-dark text-center sticky-top" id="navbar"> 
                <ul class="navbar-nav w-100"> 
                    
                    <li class="nav-item w-100">
                        <a class="nav-link d-none d-md-block delete_row" id="delete_row" onclick="{{ 'showNextRowAjax(\'' . $fullUrl . '\', \'delete\')'  }}">{{ __('Видалити запис') }}</a> 
                        <a class="nav-link d-block d-md-none delete_row" id="delete_row" onclick="{{ 'showNextRowAjax(\'' . $fullUrl . '\', \'delete\')'  }}">{{ __('x') }}</a> 
                    </li>
                    
                    <!--@ if ($ paginator->hasPages())-->
                      
                        <!--на первую страницу-->
                        <li class="nav-item w-100">
                            @if ($paginator->onFirstPage())
                                <a class="nav-link first_page" id="first_page">  &lt;&lt;</a>
                            @else
                                <!--<a class="nav-link" href="{ { $paginator->url(1) }}">  &lt;&lt;</a>--> 
                                <a class="nav-link first_page" id="first_page" onclick="{{ 'showNextRowAjax(\'' . $paginator->url(1) . '\')'  }}">  &lt;&lt;</a> 
                            @endif
                        </li>
                        
                        
                        {{-- Previous Page Link --}}
                        @if ($paginator->onFirstPage())
                            <li class="nav-item w-100 disabled" aria-disabled="true">
                                <a class="nav-link d-none d-md-block prev_page"  id="prev_page" aria-hidden="true"> &lt; {{ __('Назад') }} </a> 
                                <a class="nav-link d-block d-md-none prev_page"  id="prev_page" aria-hidden="true"> &lt;</a> 
                            </li>
                        @else
                            <li class="nav-item w-100">
                                <!--<a class="nav-link"  href="{ { $paginator->previousPageUrl() }}" rel="prev"> &lt; Назад </a>--> 
                                <a class="nav-link d-none d-md-block prev_page" id="prev_page" onclick="{{ 'showNextRowAjax(\'' . $paginator->previousPageUrl(). '\')' }}" rel="prev"> &lt; {{ __('Назад') }} </a> 
                                <a class="nav-link d-block d-md-none prev_page" id="prev_page" onclick="{{ 'showNextRowAjax(\'' . $paginator->previousPageUrl(). '\')' }}" rel="prev"> &lt; </a> 
                            </li>
                        @endif
                        
                        
                        <!--на следующую страницу-->
                        {{-- Next Page Link --}}
                        @if ($paginator->hasMorePages())                            
                            <li class="nav-item w-100">
                                <a class="nav-link d-none d-md-block next_page" id="next_page" onclick="{{ 'showNextRowAjax(\'' . $paginator->nextPageUrl() . '\')' }}" rel="next" > {{ __('Вперед') }} &gt; </a> 
                                <a class="nav-link d-block d-md-none next_page" id="next_page" onclick="{{ 'showNextRowAjax(\'' . $paginator->nextPageUrl() . '\')' }}" rel="next" > &gt; </a> 
                            </li>
                        @else                            
                            <li class="nav-item w-100 disabled" aria-disabled="true" >
                                <a class="nav-link d-none d-md-block next_page" id="next_page" aria-hidden="true"> {{ __('Вперед') }} &gt; </a> 
                                <a class="nav-link d-block d-md-none next_page" id="next_page" aria-hidden="true"> &gt; </a> 
                            </li>
                        @endif
                        
                       
                        <!--на последнюю страницу-->
                        <li class="nav-item w-100">
                            @if ($paginator->currentPage() == $paginator->lastPage())
                                <a class="nav-link last_page" id="last_page"> &gt;&gt;</a>
                            @else
                                <a class="nav-link last_page" id="last_page" onclick="{{ 'showNextRowAjax(\'' . $paginator->url($paginator->lastPage()). '\')'  }}"> &gt;&gt;</a> 
                            @endif
                        </li>
                        
                        
                        
                    <!--@ endif-->
                    
                    <li class="nav-item w-100">
                        <!--<input class="nav-link bg-secondary w-75 mx-auto" type="submit" name="save_as_new_entry" value="Зберегти як нову" form="qqq">-->            
                        <input class="nav-link bg-secondary w-full mx-auto d-none d-md-block" type="button" value="{{ __('Зберегти як нову') }}" onclick="{{ 'showNextRowAjax(\'' . $fullUrl . '\', \'save_as_new\')'  }}">            
                        <input class="nav-link bg-secondary w-full mx-auto d-block d-md-none" type="button" value="N" onclick="{{ 'showNextRowAjax(\'' . $fullUrl . '\', \'save_as_new\')'  }}">            
                    </li>
                    <li class="nav-item w-100">
                        <!--<input class="nav-link btn btn-link" type="submit" name="update_entry" value="Оновити запис" form="qqq">-->
                        <input class="nav-link bg-secondary w-full mx-auto d-none d-md-block " type="button" value="{{ __('Оновити запис') }}" onclick="{{ 'showNextRowAjax(\'' . $fullUrl . '\', \'update\')'  }}">
                        <input class="nav-link bg-secondary w-full mx-auto d-block d-md-none " type="button" value="U" onclick="{{ 'showNextRowAjax(\'' . $fullUrl . '\', \'update\')'  }}">
                    </li>
                
                
                </ul>
                
            </nav>

