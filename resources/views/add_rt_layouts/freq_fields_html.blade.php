
 @if (is_null($frequencies) || empty($frequencies))
        
    @php
        $frequencies = array( array("rf_start_mhz" => "0", "rf_stop_mhz" => "0") );
    @endphp
        
    @endif 
    
    
    
    
    @foreach ($frequencies as $row)
    
         <!--если $ key==0, то не добавляем к $ childId цифру 0.--> 
        @php
            $key = $loop->index;
            $childId = "www" . ($key==0 ? $key="" : $key); 
            $set_div_id = isset($set_div_id) ? $set_div_id : null;
        @endphp
        
        
        @include('add_rt_layouts.freq_fields_row', ['key' => $key, 'row' => $row, 'childId' => $childId, 'set_div_id' => $set_div_id])
        
    @endforeach 
    
    


