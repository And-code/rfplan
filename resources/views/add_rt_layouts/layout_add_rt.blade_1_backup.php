
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{{ __('Додавання радіотехнології') }}</title>
        <link rel="stylesheet" href="{{ asset('css/style_add_rt.css') }}">    
        
        <!-- provide the csrf token -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <!--блок для bootstrap 4-->
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
        <!--блок для bootstrap. END-->

        
        
    </head>
    <body>
        
        
        
        <div class="container-fluid"> 
        
            @section('title')
            <div class="row"> 
                <div class="col"> 
                    <h1 style="text-align: center">{{ __('Редагування Плану частот') }}</h1>
                </div>
            </div>
            @show
            
            @section('navbar_main')
            <!--Навигация-->
            <nav class="navbar navbar-expand-sm bg-success navbar-dark text-center"> 
                
                <div class="container">
                    
                    <button class="navbar-toggler" 
                                type="button" 
                                data-toggle="collapse" 
                                data-target="#navbarSupportedContent" 
                                aria-controls="navbarSupportedContent" 
                                aria-expanded="false" 
                                aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                
                    <div class="collapse navbar-collapse">

                        <ul class="navbar-nav mr-auto">


                        </ul>
                    
                    
                    </div>
                </div>
                
                <ul class="navbar-nav w-100"> 
                    
                    
                    
                    <div class="collapse navbar-collapse">
                    
                        <li class="nav-item w-100">
                            <a class="nav-link text-white" href="{{ route('main', app()->getLocale()) }}"><strong>{{ __('Повернутися на головну сторінку') }}</strong></a>
                        </li>
                        <li class="nav-item w-100">
                            <!--<a class="nav-link" href="db_management_page.php">Вибір бази даних з Планом частот</a>--> 
                            <a class="nav-link" href="{{ route('show_db_manag_page', app()->getLocale()) }}">{{ __('Керування базою даних') }}</a> 
                        </li>

                        <li class="nav-item w-75 mx-auto {{request()->url() == route('show_rt_zk_active', app()->getLocale()) ? 'active' : ''}}">
                            <a class="nav-link" href="{{ route('show_rt_zk_active', app()->getLocale()) }}">{{ __('ЗК, діючі') }}</a> 
                        </li>


                        <li class="nav-item w-75 mx-auto {{request()->url() == route('show_rt_sk_active', app()->getLocale()) ? 'active' : ''}}">
                            <a class="nav-link" href="{{ route('show_rt_sk_active', app()->getLocale()) }}">{{ __('СК, діючі') }}</a>
                        </li>


                        <li class="nav-item w-75 mx-auto {{request()->url() == route('show_rt_zk_perspective', app()->getLocale()) ? 'active' : ''}}">
                            <a class="nav-link" href="{{ route('show_rt_zk_perspective', app()->getLocale()) }}">{{ __('ЗК, перспективні') }}</a>
                        </li>

                        <li class="nav-item w-75 mx-auto {{request()->url() == route('show_rt_sk_perspective', app()->getLocale()) ? 'active' : ''}}">
                            <a class="nav-link" href="{{ route('show_rt_sk_perspective', app()->getLocale()) }}">{{ __('СК, перспективні') }}</a>
                        </li>
                    
                        
                        @section('locale')
                            @include('layouts.div_locale_switch')
                        @show
                        
                    </div>
                    
                    </ul>
                    
                
                </nav>
            @show 
            
            @yield('navbar_paginate')
                       
            @yield('message')

<div class="row">
        <div class="col add_rt_part">

            
            @yield('add_rt_part')
            
            

            
        </div>


</div>
        </div>
        
        <script src="{{ asset('js/js_functions_common_new.js') }}"></script>
        <script src="{{ asset('js/update_rt_select_list.js') }}"></script>
        
    </body>
</html>
