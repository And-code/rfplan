<div class="div_row">
    <p class="collumn_name">
        {{ __('Радіотехнологія (назва)') }}
        <span class="error">*</span>
    </p>
    <p class="collumn_value">
        <input onClick="this.setSelectionRange(0, this.value.length)"
               class="input_field" id="rt_name" name="rt_name" type="text" 
               placeholder="{{ __('Аналоговий короткохвильовий радіозв\'язок') }}"
               value="{{$first_rt->first() == null ? "": $first_rt->first()->rt_name}}">
    </p>
</div>
