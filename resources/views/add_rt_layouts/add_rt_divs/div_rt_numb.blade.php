
<div class="div_row">
    <p class="collumn_name">
        {{ __('Радіотехнологія (номер)') }}
        <span class="error">*</span>
    </p>
    <p class="collumn_value">
        <input  onClick="this.setSelectionRange(0, this.value.length)"
                class="input_field" id="rt_numb" name="rt_numb" type="text" placeholder="1" 
                value="{{$first_rt->first() == null ? "" : $first_rt->first()->rt_numb}}">
    </p>
</div>