<div class="div_row">
    <p class="collumn_name">
        @if ($rt_active_perspect_type == 1) 
            {{ __('Радіослужба') }}
        @else
            {{ __('Радіослужба, за якою планується використання радіотехнології') }}
        @endif
        <span class="error">*</span>
    </p>
    <p class="collumn_value">

        <textarea onfocus="this.setSelectionRange(0, this.value.length)"
                  class="input_field" id="rs_name" name="rs_name" 
                  placeholder="{{ __('сухопутна рухома') }}"
                  rows="1" cols="50" >{{$first_rt->first() == null ? "" : $first_rt->first()->rs_name}}</textarea>
    </p>
</div>