<div class="div_row">
    <p class="collumn_name">
        {{ __('Дата початку дії (наприклад, після того, як внесли зміни в запис за рішенням КМУ або Указу Президента)') }}
    </p>
    <p class="collumn_value">
        <input class="input_field" id="gov_accept_date" name="gov_accept_date" type="date"
               value="{{$first_rt->first()==null ? "": $first_rt->first()->gov_accept_date}}">
    </p>
</div>

