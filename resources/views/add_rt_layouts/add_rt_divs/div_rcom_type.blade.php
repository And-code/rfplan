<div class="div_row">
    <p class="collumn_name">
        {{ __('Вид радіозвʼязку') }}
    </p>
    <p class="collumn_value">

        <textarea onfocus="this.setSelectionRange(0, this.value.length)"
                  class="input_field" id="rcom_type" name="rcom_type" 
                  placeholder="{{ __('радіозв\'язок фіксованої, рухомої сухопутної та морської радіослужб') }}"
                  rows="4" cols="50" >{{$first_rt->first() == null ? "" : $first_rt->first()->rcom_type}}</textarea>        
    </p>
</div>