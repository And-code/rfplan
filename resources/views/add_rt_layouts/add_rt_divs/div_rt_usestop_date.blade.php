<div class="div_row">
    <p class="collumn_name">
        {{ __('Строк припинення використання радіотехнології') }}
    </p>
    <p class="collumn_value">

        <textarea onfocus="this.setSelectionRange(0, this.value.length)"
                  class="input_field" id="rt_usestop_date" name="rt_usestop_date" 
                  placeholder="xx.xx.xxxx"
                  rows="1" cols="50" >{{$first_rt->first() == null ? "": $first_rt->first()->rt_usestop_date}}</textarea>
    </p>
</div>