<div class="div_row">
    <!--<p class="collumn_name">-->
    <p class="row">
        {{ __('Смуга радіочастот') }}
        <!--<span class="error"> </span>-->
    </p>


    <!--<div class="collumn_value">-->
    <!--<div class="collumn_value_freq" >-->
    <div class="row" >

        <div id="freqDiv"> 

            
            @section('freq_fields_html')
                @include('add_rt_layouts.freq_fields_html', ['frequencies' => $frequencies])
            @show
            
        </div>

        <div class="row"> 
            <div class="col text-right">
                <div class="btn btn-sm btn-info w-100 mb-1" onclick="addFreqField()">{{ __('Додати') }}</div>

            </div>
        </div>
    </div>

</div>