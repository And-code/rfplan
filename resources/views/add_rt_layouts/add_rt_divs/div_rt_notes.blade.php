<div class="div_row">
    <p class="collumn_name">
        {{ __('Примітки щодо радіотехнології') }}
    </p>
    <p class="collumn_value">

        <textarea onfocus="this.setSelectionRange(0, this.value.length)"
                  class="input_field" id="rt_notes" name="rt_notes" 
                  placeholder="{{ __('Наприклад: {Дію окремих положень позиції 19 розділу I зупинено... ') }}"
                  rows="4" cols="50" >{{$first_rt->first() == null ? "": $first_rt->first()->rt_notes}}</textarea>
    </p>
</div>
