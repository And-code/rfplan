<div class="div_row">
    <p class="collumn_name">        
        {{ __('Положення РР МСЕ, резолюції ВКР, рекомендації МСЕ, СЕПТ, рішення ЄКК, міжнародні угоди, акти законодавства ЄС') }}
    </p>
    <p class="collumn_value">

        <textarea onfocus="this.setSelectionRange(0, this.value.length)"
                  class="input_field" id="other_norms" name="other_norms" 
                  placeholder="ERC/DEC (98)11 ERC/REC T/R 20-09"
                  rows="4" cols="50">{{$first_rt->first() == null ? "" : $first_rt->first()->other_norms}}</textarea>

    </p>
</div>