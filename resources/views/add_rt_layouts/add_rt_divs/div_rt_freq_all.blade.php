                <div class="div_row">
                    <p class="collumn_name">
                        {{ __('Смуга радіочастот (вставити список частот як в Плані частот)') }}
                    </p>
                    <p class="collumn_value">
                        
                        <textarea onfocus="this.setSelectionRange(0, this.value.length)"
                            class="input_field" id="rt_freq_all" name="rt_freq_all" 
                                  placeholder="1-2 МГц"
                                  rows="4" cols="50" >{{$first_rt->first() == null ? "" : $first_rt->first()->rt_freq_all}}</textarea>
                    </p>
                </div>