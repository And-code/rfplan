<div class="div_row">
    <p class="collumn_name">


        
        @if ($rt_active_perspect_type == 1) 
            {{ __('Особливості застосування радіотехнологій') }}
        @else
            {{ __('Особливості впровадження радіотехнологій') }}
        @endif
        


    </p>
    <p class="collumn_value">

        <textarea onfocus="this.setSelectionRange(0, this.value.length)"
                  class="input_field" id="rt_use_features" name="rt_use_features" 
                  placeholder="Т01, Д01"
                  rows="4" cols="50" >{{$first_rt->first() == null ? "": $first_rt->first()->rt_use_features}}</textarea>

    </p>
</div>