<div class="div_row">
    <p class="collumn_name">
        {{ __('Базові стандарти') }}
    </p>
    <p class="collumn_value">

        <textarea onfocus="this.setSelectionRange(0, this.value.length)"
                  class="input_field" id="std_base" name="std_base" 
                  placeholder="ГОСТ 22579 ГОСТ 14663 ГОСТ 13420"
                  rows="4" cols="50" >{{$first_rt->first() == null ? "": $first_rt->first()->std_base}}</textarea>

    </p>
</div>