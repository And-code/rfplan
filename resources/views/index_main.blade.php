@extends('layouts.layoutMain')

@section('title')
    @parent
@endsection

@section('form')
    @parent
@endsection

@section('rt_table')
    @parent
@endsection