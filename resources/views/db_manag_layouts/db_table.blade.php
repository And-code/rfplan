    


<br>
<!-- --- Заголовок таблицы ------>
<table class='zk_table_style table table-bordered' >
    <tr>
        <th class='zk_table_style_th_td'>№</th>
        <th class='zk_table_style_th_td'>{{ __('Аліас таблиці') }}</th>
        <th class='zk_table_style_th_td'>{{ __('Дата затвердження Плану частот') }}</th>
        <th class='zk_table_style_th_td'>{{ __('Опис редакції') }}</th>
        <th class='zk_table_style_th_td'>{{ __('Обрана?') }}</th>
        <th class='zk_table_style_th_td'>{{ __('Дії') }}</th>
    </tr>
    <!--// --- Заголовок таблицы -- КОНЕЦ------>


    <!--// --- тело таблицы------->
    @foreach ($descriptions as $description) 

    <tr>
        <!--// номер порядковый--> 
        <td class='zk_table_style_th_td'> {{ $loop->iteration }}
            <!--$key+1 . "&nbsp";-->

            <!--// имя базы данных-->
        </td>           
        <td class='zk_table_style_th_td'>
            <!--<button class="btn btn-info" onclick="">Змінити</button>-->            

<!--<input class="form-control bg-white input_field_mange_db" -->            
            <input class="form-control bg-white" 
                   style="min-width:150px"
                   type="text" 
                   id="{{ 'p_set_table_name_' . $description->rfplan_table_name}}" 
                   value="{{ $description->alias }}"
                   >

        </td>

        <!--$db_name = $db_list[$key];-->

        <!--// дата утверждения Плана частот-->
        <td class='zk_table_style_th_td'>
            <!--<button class="btn btn-info" onclick="">Змінити</button>-->            

            <input class="form-control bg-white input_field_mange_db" 
                   style="min-width:150px"
                   type="text" 
                   id="{{ 'p_set_date_id_'.$description->rfplan_table_name}}" 
                   value="{{ $description->rfplan_date }}"
                   >

        </td>

        <!--// описание базы данных-->
        <td class='zk_table_style_th_td'>
            <!--<button class="btn btn-info" onclick="">Змінити</button>-->

            <textarea class="form-control bg-white input_field_mange_db" class="input_field" 
                      id="{{ 'p_set_descr_id_'.$description->rfplan_table_name}}" 
                      style="min-width:300px"
                      rows="4" 
                      cols="50" 
                      >{{ $description->plan_release_descr }}</textarea>

        </td>

        <!--// выбрана как текущая или нет-->
        <td id="{{ 'td_id_'.$description->rfplan_table_name}}" class='zk_table_style_th_td'>
            {{ session('rfplan_rows')== $description->rfplan_table_name ? __('Так') : __('Ні') }}
        </td>

        <!--            // действия
                    // action_type: 'select', 'delete', 'clone', 'export', 'export_all', 'create_new', 'import'-->

        <td class='zk_table_style_th_td'>
            <div class='btn-group-vertical btn-group-sm'>

                <a class="btn btn-info" onclick="{{ 'db_manag_buttons_func(\'select\', \'' . $description->rfplan_table_name. '\')'}}" href="#">{{ __('Обрати') }}</a>
                <a class="btn btn-info" onclick="{{ 'db_manag_buttons_func(\'update\', \'' . $description->rfplan_table_name. '\')'}}" href="#">{{ __('Зберегти') }}</a>

                <a class="btn btn-info" onclick="{{ 'db_manag_buttons_func(\'delete\', \'' . $description->rfplan_table_name. '\')'}}" href="#">{{ __('Видалити') }}</a>
                <a class="btn btn-info" onclick="{{ 'db_manag_buttons_func(\'clone\', \'' . $description->rfplan_table_name. '\')'}}" href="#">{{ __('Клонувати') }}</a>

            </div>

        </td>

    </tr>

    @endforeach

    <!--// --- тело таблицы--КОНЕЦ----->
    <!--<br>-->
