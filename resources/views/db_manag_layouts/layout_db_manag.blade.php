

<!--require_once './get_from_db.php';-->
<!--доделать страницу!-->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{{ __('Керування базою даних') }}</title>
        <link rel="stylesheet" href="{{ asset('css/style_add_rt.css') }}">    
        
        <!-- provide the csrf token -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <!--блок для bootstrap 4-->
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
        <!--блок для bootstrap. END-->

         <!--проверить нужно ли это-->
        <script src="{{ asset('js/js_functions_common_new.js') }}"></script>
        <script src="{{ asset('js/db_management.js') }}"></script>
        
    </head>
    <body>
        
        
        
        <div class="container-fluid"> 
        
            @section('title')
            <div class="row"> 
                <div class="col "> 
                    <h1 class="my-title" style="text-align: center;">{{ __('Керування базою даних') }}</h1>
                </div>
            </div>
            @show
            
            @section('navbar_main')
            <!--Навигация-->
            <nav class="navbar navbar-expand-md bg-success navbar-dark text-center" id="navbar_main"> 
                
                 <a class="text-white navbar-text" href="{{ route('main', app()->getLocale()) }}"><strong>{{ __('На головну сторінку') }}</strong></a>
                    
                    <button class="navbar-toggler" 
                                type="button" 
                                data-toggle="collapse" 
                                data-target="#navbarSupportedContent" 
                                aria-controls="navbarSupportedContent" 
                                aria-expanded="false" 
                                aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                    
                            </button>
                
                 <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     
                     
                        <ul class="navbar-nav w-100"> 

        <!--                    <li class="nav-item w-100">
                                <a class="nav-link text-white" href="{ { route('main', app()->getLocale()) }}"><strong>{ { __('Повернутися на головну сторінку') }}</strong></a>
                            </li>-->
                            <li class="nav-item mx-auto">
                                <a class="nav-link" href="{{ route('show_rt_zk_active', app()->getLocale()) }}">{{ __('Редагування Плану частот') }}</a> 
                            </li>

                            <li class="nav-item mx-auto">
                                <a class="nav-link" onclick="{{ 'db_manag_buttons_func(\'create\', null )'}}" href="#">{{ __('Створити новий План частот') }}</a> 
                            </li>

                            @if (Route::has('register'))
                                <li class="nav-item mx-auto">
                                    <a class="nav-link" href="{{ route('register', app()->getLocale()) }}">{{ __('Реєстрація') }}</a>
                                </li>
                            @endif

                             <li class="nav-item mx-auto">
                                @section('locale')
                                    @include('layouts.div_locale_switch')
                                @show
                            </li>
                            
                        </ul>
                        
                     
                 </div>
                 
                 
                </nav>
            @show 
                                   
            @yield('message')

            <!--Таблица со списком баз данных--> 
<div class="row" >
    <div class="col overflow-auto" id="div_db_table">
    

            
            @yield('db_table')
            
            

            
    
        </div>


</div>
        </div>
        
    </body>
</html>
