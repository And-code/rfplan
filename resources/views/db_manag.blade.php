@extends('db_manag_layouts.layout_db_manag')

@section('title')
    @parent
@endsection

@section('navbar_main')
    @parent
@endsection

@section('message')
    @if ($type == 'danger')
        @include('layouts.div_message')
    @endif
@endsection


@section('db_table')
   
@if ($type != 'danger')
   
        @include('db_manag_layouts.db_table')
   
@endif
    
@endsection