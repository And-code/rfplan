@extends('add_rt_layouts.layout_add_rt')

@section('title')
    @parent
@endsection

@section('navbar')
    @parent
@endsection

@section('navbar_paginate')
    <!--@ include('add_rt_layouts.navbar_paginate_add_rt')-->                                                                       
    @if ($type != 'danger')
        {{ $first_rt->links(null, ['fullUrl' => $fullUrl]) }}
    @endif
@endsection

@section('message')
    @if ($type == 'danger')
        @include('layouts.div_message')
    @endif
@endsection


@section('add_rt_part')
   
@if ($type != 'danger')
    <!--zk_active-->
    @if ($user_type == 1 && $rt_active_perspect_type == 1)
    
        @include('add_rt_layouts.fields_add_rt', ['user_type' => 1, 'rt_active_perspect_type' => 1])
    
    <!--sk_active-->
    @elseif ($user_type == 0 && $rt_active_perspect_type == 1)
        @include('add_rt_layouts.fields_add_rt', ['user_type' => 0, 'rt_active_perspect_type' => 1])    
            
    <!--zk_perspective-->
    @elseif ($user_type == 1 && $rt_active_perspect_type == 0)
        @include('add_rt_layouts.fields_add_rt', ['user_type' => 1, 'rt_active_perspect_type' => 0])    
                
    <!--sk_perspective-->
    @elseif ($user_type == 0 && $rt_active_perspect_type == 0)
        @include('add_rt_layouts.fields_add_rt', ['user_type' => 0, 'rt_active_perspect_type' => 0])    
    @endif

@endif
    
@endsection